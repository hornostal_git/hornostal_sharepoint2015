﻿/**
 * @author Алексей Горносталь
 */
using System;

namespace TaskWeb02
{
    public partial class Default : System.Web.UI.Page
    {
        #region Методы

        protected override void OnPreInit(EventArgs e)
        {
            Page.Response.Write(string.Format("OnPreInit: {0}<br/>", DateTime.Now.ToString()));
            base.OnPreInit(e);
        }

        protected override void OnInit(EventArgs e)
        {
            Page.Response.Write(string.Format("OnInit: {0}<br/>", DateTime.Now.ToString()));
            base.OnInit(e);
        }

        protected override void OnInitComplete(EventArgs e)
        {
            Page.Response.Write(string.Format("OnInitComplete: {0}<br/>", DateTime.Now.ToString()));
            base.OnInitComplete(e);
        }

        protected override void OnPreLoad(EventArgs e)
        {
            Page.Response.Write(string.Format("OnPreLoad: {0}<br/>", DateTime.Now.ToString()));
            base.OnPreLoad(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            Page.Response.Write(string.Format("OnLoad: {0}<br/>", DateTime.Now.ToString()));
            base.OnLoad(e);
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            Page.Response.Write(string.Format("OnLoadComplete: {0}<br/>", DateTime.Now.ToString()));
            base.OnLoadComplete(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            Page.Response.Write(string.Format("OnPreRender: {0}<br/>", DateTime.Now.ToString()));
            base.OnPreRender(e);
        }

        protected override void OnPreRenderComplete(EventArgs e)
        {
            Page.Response.Write(string.Format("OnPreRenderComplete: {0}<br/>", DateTime.Now.ToString()));
            base.OnPreRenderComplete(e);
        }

        protected override void OnSaveStateComplete(EventArgs e)
        {
            Page.Response.Write(string.Format("OnSaveStateComplete: {0}<br/>", DateTime.Now.ToString()));
            base.OnSaveStateComplete(e);
        }

        // На данном этапе рендеринг уже завершён, поэтому отображение лога на странице уже невозможно.
        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
            //Page.Response.Write(string.Format("OnUnload: {0}<br/>", DateTime.Now.ToString()));
        }

        #endregion

        #region События

        protected void Page_PreInit(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_PreInit: {0}<br/>", DateTime.Now.ToString()));
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_Init: {0}<br/>", DateTime.Now.ToString()));
        }

        protected void Page_InitComplete(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_InitComplete: {0}<br/>", DateTime.Now.ToString()));
        }

        protected void Page_PreLoad(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_PreLoad: {0}<br/>", DateTime.Now.ToString()));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_Load: {0}<br/>", DateTime.Now.ToString()));
            if (Page.IsPostBack)
            {
                Page.Response.Write("<b>Это PostBack!</b><br/>");
            }
            if (Page.Request.Form.Count != 0)
            {
                Response.Write("<table border='1px'><tr><td>#</td><td>Элементы из Page.Request.Form</td></tr>");
                for (int i = 0; i < Page.Request.Form.Count; i++)
                {
                    Response.Write(string.Format("<tr><td>{0}</td><td>{1}</td></tr>", i, Page.Request.Form[i]));
                }
                Response.Write("</table>");
            }
            
        }

        protected void butonPostBack_Click(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("<b>butonPostBack_Click: {0}</b><br/>", DateTime.Now.ToString()));
        }

        protected void Page_LoadComplete(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_LoadComplete: {0}<br/>", DateTime.Now.ToString()));
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_PreRender: {0}<br/>", DateTime.Now.ToString()));
        }

        protected void Page_PreRenderComplete(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_PreRenderComplete: {0}<br/>", DateTime.Now.ToString()));
        }      

        protected void Page_SaveStateComplete(object sender, EventArgs e)
        {
            Page.Response.Write(string.Format("Page_SaveStateComplete: {0}<br/>", DateTime.Now.ToString()));
        }

        // На данном этапе рендеринг уже завершён, поэтому отображение лога на странице уже невозможно.
        protected void Page_Unload(object sender, EventArgs e)
        {
            //Page.Response.Write(string.Format("Page_Unload: {0}<br/>", DateTime.Now.ToString()));
        }
        
        #endregion
    }
}
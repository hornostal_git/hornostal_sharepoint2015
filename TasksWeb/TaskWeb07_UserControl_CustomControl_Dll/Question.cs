﻿/**
 * @author Алексей Горносталь
 */
namespace TaskWeb07_UserControl_CustomControl_Dll
{
    [System.Serializable]
    // Вопрос
    public class Question : System.EventArgs
    {
        // Содержание вопроса
        public string Title { get; set; }

        // Список овтетов на вопрос
        public System.Collections.Generic.List<Answer> Answers { get; set; }

        // Получен ил ответ на вопрос?
        public bool IsAnswered;

        // индекс парвильного ответа в списке
        private int _correctAnswerIndex;

        // Обеспечивает корректный доступ к индексу правильного овтета
        public int CorrectAnswerIndex
        {
            get
            {
                return _correctAnswerIndex;
            }
            set
            {
                if (value > 0 && value < Answers.Count)
                {
                    _correctAnswerIndex = value;
                }
            }
        }
    }
}

﻿/**
 * @author Алексей Горносталь
 */
using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace TaskWeb07_UserControl_CustomControl_Dll
{
    // Серверный элемент управления для проведения тестирования
    public class QuestionsCustomControl : CompositeControl
    {
        #region Доступные извне свойства Custom Control'а

        public List<Question> Questions
        {
            get { return (List<Question>)ViewState["Questions"]; }
            set { ViewState["Questions"] = value; }
        }

        public string QuestionFontSize
        {
            get
            {
                if (ViewState["QuestionFontSize"] != null)
                {
                    return (string)ViewState["QuestionFontSize"];
                }
                else
                {
                    return "16px";
                }
            }
            set { ViewState["QuestionFontSize"] = value; }
        }

        public string AnswerFontSize
        {
            get
            {
                if (ViewState["AnswerFontSize"] != null)
                {
                    return (string)ViewState["AnswerFontSize"];
                }
                else
                {
                    return "16px";
                }
            }
            set { ViewState["AnswerFontSize"] = value; }
        }

        #endregion

        private int AnsweredQuestionsCount
        {
            get
            {
                return Questions.FindAll(q => q.IsAnswered).Count;
            }
        }


        #region Элементы управления Custom Control'а

        Button button;
        List<HtmlGenericControl> rblist = new List<HtmlGenericControl>();
        Label labelSummary;
        MultiView multiView;

        #endregion


        protected override void CreateChildControls()
        {
            multiView = new MultiView();
            View firstView = new View();
            View secondView = new View();

            HtmlGenericControl div;
            RadioButtonList rbl;

            if (Questions != null)
            {
                foreach (var question in Questions)
                {
                    div = new HtmlGenericControl("div");
                    div.Style.Add(HtmlTextWriterStyle.Width, "300px");
                    div.Style.Add(HtmlTextWriterStyle.BorderStyle, "solid");
                    div.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                    Label title = new Label { Text = question.Title };
                    title.Style.Add(HtmlTextWriterStyle.FontSize, QuestionFontSize);
                    div.Controls.Add(title);

                    rbl = new RadioButtonList();

                    foreach (var answer in question.Answers)
                    {
                        rbl.Items.Add(answer.Content);
                    }

                    var selected = question.Answers.Find(a => a.IsSelected);
                    var selectedIndex = question.Answers.IndexOf(selected);
                    if (selectedIndex != -1)
                    {
                        rbl.Items[selectedIndex].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "red");
                    }

                    rbl.ToolTip = question.Title;
                    div.Controls.Add(rbl);
                    rbl.SelectedIndexChanged += Rbl_SelectedIndexChanged;
                    rbl.Style.Add(HtmlTextWriterStyle.FontSize, QuestionFontSize);
                    rbl.AutoPostBack = true;
                    rblist.Add(div);
                    firstView.Controls.Add(div);
                }
                labelSummary = new Label { Text = "Всего вопросов: " + Questions.Count + "\nОтвечено: " + AnsweredQuestionsCount };
                firstView.Controls.Add(labelSummary);
                firstView.Controls.Add(new LiteralControl("<br/>"));
                button = new Button {Text = "Закончить тест!"};
                button.Click += Submit_Click;
                button.UseSubmitBehavior = false;

                firstView.Controls.Add(button);
                secondView.Controls.Add(new Label { Text = "Ваши результаты отправлены на проверку!" });
                multiView.Views.Add(firstView);
                multiView.Views.Add(secondView);
                multiView.ActiveViewIndex = 0;
                this.Controls.Add(multiView);
            }
        }


        #region Методы обработки взаимодействия пользователя с элементами управления

        private void Rbl_SelectedIndexChanged(object sender, EventArgs e)
        {
            RadioButtonList rbl = (RadioButtonList)sender;
            string question = rbl.ToolTip;
            string answer = rbl.Text;

            var x = Questions.Find(q => q.Title.Equals(question));
            x.IsAnswered = true;
            int previousAnswerIndex = x.Answers.FindIndex(n => n.IsSelected);
            foreach (var ans in x.Answers)
            {
                ans.IsSelected = false;
            }
            
            Answer currentAnswer = x.Answers.Find(n => n.Content.Equals(answer));
            currentAnswer.IsSelected = true;
            int selectedAnswerIndex = x.Answers.FindIndex(a => a.IsSelected);
            rbl.Items[selectedAnswerIndex].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "red");
            if (previousAnswerIndex != -1)
            {
                rbl.Items[previousAnswerIndex].Attributes.CssStyle.Add(HtmlTextWriterStyle.Color, "black");
            }
            labelSummary.Text = "Всего вопросов: " + Questions.Count + "\nОтвечено: " + AnsweredQuestionsCount;
            OnIndexChanged(new Question { Title = question, Answers = new List<Answer> { new Answer(answer) } });
        }
        
        private void Submit_Click(object sender, EventArgs e)
        {
            if (AnsweredQuestionsCount!=Questions.Count)
            {
                labelSummary.Text += "\nНужно ответить на все вопросы!";
                return;
            }
            foreach (var div in rblist)
            {
                string text = "";
                string answer = "";
                foreach (var n in div.Controls)
                {
                    if (n is Label)
                    {
                        text = ((Label)n).Text;
                    }
                    if (n is RadioButtonList)
                    {
                        answer = ((RadioButtonList)n).SelectedItem.Text;
                    }
                }
            }
            multiView.ActiveViewIndex = 1;
            OnSubmitClick(EventArgs.Empty);            
        }

        #endregion


        #region События и методы для их обработки

        public event EventHandler TestEnded;
        private void OnSubmitClick(EventArgs e)
        {
            TestEnded?.Invoke(this, e);
        }

        public event EventHandler QuestionAnswered;
        private void OnIndexChanged(EventArgs e)
        {
            QuestionAnswered?.Invoke(this, e);
        }

        #endregion
    }
}
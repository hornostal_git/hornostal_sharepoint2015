﻿/**
 * @author Алексей Горносталь
 */
namespace TaskWeb07_UserControl_CustomControl_Dll
{
    [System.Serializable]
    // Ответ на вопрос
    public class Answer
    {
        public Answer(string content)
        {
            Content = content;
        }

        // Содержания ответа
        public string Content;

        // Выбран ли овтет пользователем
        public bool IsSelected;
    }
}

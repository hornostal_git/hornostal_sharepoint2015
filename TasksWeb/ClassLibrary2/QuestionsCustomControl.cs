﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace TaskWeb07_UserControl_CustomControl_Dll
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class QuestionsCustomControl : CompositeControl, IPostBackEventHandler
    {
        public List<Question> Questions { get { return (List<Question>)ViewState["Questions"]; } set { ViewState["Questions"] = value; } }

        public QuestionsCustomControl()
        {

        }

        protected override void CreateChildControls()
        {
            HtmlGenericControl div;
            RadioButtonList rbl;

            if (Questions != null)
            {
                foreach (var question in Questions)
                {
                    div = new HtmlGenericControl("div");
                    //div.Style.Add(HtmlTextWriterStyle.Display, "inline-block");
                    div.Style.Add(HtmlTextWriterStyle.Width, "300px");
                    div.Style.Add(HtmlTextWriterStyle.BorderStyle, "solid");
                    div.Style.Add(HtmlTextWriterStyle.BorderWidth, "1px");
                    div.Controls.Add(new Label { Text = question.Title });

                    rbl = new RadioButtonList();
                    foreach (var answer in question.Answers)
                    {
                        rbl.Items.Add(answer);
                    }
                    div.Controls.Add(rbl);

                    this.Controls.Add(div);
                    //this.Controls.Add(new LiteralControl("<br/>"));
                }
                Button submit = new Button();
                submit.Text = "Закончить тест!";
                submit.Click += Submit_Click;

            this.Controls.Add(submit);
            }
        }

        private void Submit_Click(object sender, System.EventArgs e)
        {
            this.Controls.Add(new Label { Text = "Ваши результаты отправлены на проверку!" });
        }

        #region IPostBackEventHandler Members
        public event EventHandler Click;

        public virtual void OnClick(EventArgs e)
        {
            if (Click != null)
            {
                Click(this, e);
            }
        }

        public void RaisePostBackEvent(string eventArgument)
        {
            OnClick(EventArgs.Empty);
        }

        #endregion
    }
}
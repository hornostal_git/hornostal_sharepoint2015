﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;

namespace TaskWeb09_AJAX_UpdatePanel_WebServices.Models
{
    class Repository
    {
        public static string PathToFile;

        public static List<Product> GetProducts()
        {
            var productss = XDocument.Load(PathToFile).Descendants("Product");
            var x = from emp in productss
                    select new Product
                    {
                        ID = int.Parse(emp.Element("ID").Value),
                        Name = emp.Element("Name").Value,
                        Price = double.Parse(emp.Element("Price").Value.Replace('.', ',')),
                        ImageName = emp.Element("ImageName").Value,
                        ImageFullPath = "~/Resources/Images/" + emp.Element("ImageName").Value
                    };
            return x.ToList();
        }

        public static void DeleteProduct(int ID)
        {
            XDocument doc = XDocument.Load(PathToFile);
            doc.Root.Elements("Product")
                .Where(l => l.Element("ID").Value.Equals(ID.ToString()))
                .Remove();
            doc.Save(PathToFile);
        }

        public void UpdateProduct(int ID, string Name, double Price, string ImageName)
        {
            XDocument doc = XDocument.Load(PathToFile);
            var el = doc.Root.Elements("Product");
            var el2 = el.Where(l => l.Element("ID").Value.Equals(ID.ToString())).First();
            el2.Element("Name").Value = Name;
            el2.Element("Price").Value = Price.ToString(CultureInfo.InvariantCulture);
            el2.Element("ImageName").Value = ImageName;
            doc.Save(PathToFile);
        }

        public void AddProduct(string Name, double Price, string ImageName)
        {
            XDocument doc = XDocument.Load(PathToFile);
            int id = XDocument.Load(PathToFile).Descendants("Product").Max(p => int.Parse(p.Element("ID").Value)) + 1;
            doc.Root.Add(
            new XElement("Product",
                        new XElement("ID", id),
                        new XElement("Name", Name),
                        new XElement("Price", Price.ToString(CultureInfo.InvariantCulture)),
                        new XElement("ImageName", ImageName)
                        ));
            doc.Save(PathToFile);
        }
    }
}

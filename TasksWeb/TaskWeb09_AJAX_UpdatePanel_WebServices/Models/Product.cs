﻿// Товар
namespace TaskWeb09_AJAX_UpdatePanel_WebServices.Models
{
    public class Product
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string ImageName { get; set; }
        public string ImageFullPath { get; set; }
    }
}

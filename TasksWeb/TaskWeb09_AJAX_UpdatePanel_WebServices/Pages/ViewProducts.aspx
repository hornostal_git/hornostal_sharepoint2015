﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewProducts.aspx.cs" Inherits="TaskWeb09_AJAX_UpdatePanel_WebServices.Pages.ViewProducts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
        .hidden{
            display:none;
        }
        .shown{
            display:table-cell;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <asp:ScriptManager ID="ScriptManagerMain" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelMain" runat="server">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>

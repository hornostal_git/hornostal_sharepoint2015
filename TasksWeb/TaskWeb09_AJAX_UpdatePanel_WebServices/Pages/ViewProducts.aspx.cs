﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using TaskWeb09_AJAX_UpdatePanel_WebServices.Models;

namespace TaskWeb09_AJAX_UpdatePanel_WebServices.Pages
{
    public partial class ViewProducts : Page
    {
        const int PageSize = 3;
        private int MaxPage()
        {
            return (int)Math.Ceiling(new ProductsWebService().GetProducts().Count / (double)PageSize);
        }

        ImageButton _iButtonPrev;
        ImageButton _iButtonNext;
        Table _table = new Table();

        static List<Product> _currentProducts = new List<Product>();

        private List<Product> GetProducts(int page)
        {
            page--;
            return new ProductsWebService().GetProducts().Skip(page * PageSize).Take(PageSize).ToList();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _table = new Table();
            if (!IsPostBack)
            {
                Session["CurrentPage"] = 1;
                Session["LastLoadedPage"] = 1;

                var products = new ProductsWebService().GetProducts().Take(PageSize);
                _currentProducts = new List<Product>(products);
            }

            _iButtonPrev = new ImageButton
            {
                ID = "button-prev",
                ImageUrl = "~/Resources/Images/arrow-left.png",
                Width = 15,
                Height = 15
            };
            _iButtonPrev.Click += IButtonPrev_Click;
            if ((int)Session["CurrentPage"]==1)
            {
                _iButtonPrev.Attributes["class"] = "hidden";
            }
            TableRow row = new TableRow();
            _table.Rows.Add(row);
            TableCell cellPrev = new TableCell { Width = 20 };
            cellPrev.Controls.Add(_iButtonPrev);
            row.Cells.Add(cellPrev);
            foreach (var product in _currentProducts)
            {
                TableCell cell = new TableCell();
                row.Cells.Add(cell);
                Image image1 = new Image { ImageUrl = product.ImageFullPath, Width = 100, Height = 100 };
                Label label1 = new Label { Text = product.Name, ForeColor = System.Drawing.Color.DarkBlue };
                Label label2 = new Label { Text = product.Price.ToString(CultureInfo.InvariantCulture), ForeColor = System.Drawing.Color.Green, BackColor = System.Drawing.Color.LemonChiffon };
                Panel panel1 = new Panel();

                label1.Style.Add(HtmlTextWriterStyle.TextDecoration, "underline");
                label2.Style.Add(HtmlTextWriterStyle.FontWeight, "bold");

                panel1.Controls.Add(image1);
                panel1.Controls.Add(new LiteralControl("<br />"));
                panel1.Controls.Add(label1);
                panel1.Controls.Add(new LiteralControl("<br />"));
                panel1.Controls.Add(label2);
                cell.Controls.Add(panel1);
            }
            TableCell cellNext = new TableCell();
            _iButtonNext = new ImageButton
            {
                ID = "button-next",
                ImageUrl = "~/Resources/Images/arrow-right.png",
                Width = 15,
                Height = 15
            };
            _iButtonNext.Click += IButtonNext_Click;
            cellNext.Controls.Add(_iButtonNext);
            row.Cells.Add(cellNext);
            UpdatePanelMain.ContentTemplateContainer.Controls.Add(_table);
        }

        private void IButtonPrev_Click(object sender, ImageClickEventArgs e)
        {
            Session["CurrentPage"] = (int)Session["CurrentPage"] - 1;
            if ((int)Session["CurrentPage"] == 1)
            {
                _iButtonPrev.Attributes["class"] = "hidden";
            }
            _iButtonNext.Attributes["class"] = "shown";

            int firstIndexPrevPage = ((int)Session["CurrentPage"] - 1) * PageSize + 1;
            for(int i=0;i<PageSize;i++)
            {
                if (_table.Rows[0].Cells[firstIndexPrevPage] != null)
                {
                    _table.Rows[0].Cells[firstIndexPrevPage].Attributes["class"] = "shown";
                }
                firstIndexPrevPage++;
            }
            

            int firstIndex = ((int)Session["CurrentPage"]) * PageSize + 1;
            for (int i = 0; i < PageSize; i++)
            {
                if (firstIndex < _table.Rows[0].Cells.Count-1)
                {
                    _table.Rows[0].Cells[firstIndex].Attributes["class"] = "hidden";
                }
                firstIndex++;
            }
            
        }

        private void IButtonNext_Click(object sender, ImageClickEventArgs e)
        {
            Session["CurrentPage"] = (int)Session["CurrentPage"] + 1;
            _iButtonPrev.Attributes["class"] = "shown";
            if ((int)Session["CurrentPage"] == MaxPage())
            {
                _iButtonNext.Attributes["class"] = "hidden";
            }

            if ((int)Session["LastLoadedPage"] < (int)Session["CurrentPage"])
            {
                Session["LastLoadedPage"] = (int)Session["LastLoadedPage"] + 1;
                var products = GetProducts((int)Session["CurrentPage"]);

                _currentProducts.AddRange(products);
                foreach (var product in products)
                {
                    TableCell cell = new TableCell();
                    _table.Rows[0].Cells.AddAt(_table.Rows[0].Cells.Count - 1, cell);
                    Image image1 = new Image { ImageUrl = product.ImageFullPath, Width = 100, Height = 100 };
                    Label label1 = new Label { Text = product.Name, ForeColor = System.Drawing.Color.DarkBlue };
                    Label label2 = new Label { Text = product.Price.ToString(CultureInfo.InvariantCulture), ForeColor = System.Drawing.Color.Green, BackColor = System.Drawing.Color.LemonChiffon };
                    Panel panel1 = new Panel();

                    label1.Style.Add(HtmlTextWriterStyle.TextDecoration, "underline");
                    label2.Style.Add(HtmlTextWriterStyle.FontWeight, "bold");

                    panel1.Controls.Add(image1);
                    panel1.Controls.Add(new LiteralControl("<br />"));
                    panel1.Controls.Add(label1);
                    panel1.Controls.Add(new LiteralControl("<br />"));
                    panel1.Controls.Add(label2);
                    cell.Controls.Add(panel1);
                }
            }
            else
            {
                int firstIndex = ((int)Session["CurrentPage"] - 1) * PageSize + 1;
                for (int i = 0; i < PageSize; i++)
                {
                    if (firstIndex < _table.Rows[0].Cells.Count-1)
                    {
                        _table.Rows[0].Cells[firstIndex].Attributes["class"] = "shown";
                    }
                    firstIndex++;
                }
            }
            int firstIndexPrevPage = ((int)Session["CurrentPage"] - 2) * PageSize + 1;
            for (int i = 0; i < PageSize; i++)
            {
                if (_table.Rows[0].Cells[firstIndexPrevPage] != null)
                {
                    _table.Rows[0].Cells[firstIndexPrevPage].Attributes["class"] = "hidden";
                }
                firstIndexPrevPage++;
            }
        }
    }
}
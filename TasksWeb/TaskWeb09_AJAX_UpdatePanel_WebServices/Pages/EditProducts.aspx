﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditProducts.aspx.cs" Inherits="TaskWeb09_AJAX_UpdatePanel_WebServices.Pages.ProductsTable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView HeaderStyle-BackColor="#0066ff" AlternatingRowStyle-BackColor="#33ccff" ID="MainGridView" EnableViewState="False" ShowFooter="True" DataSourceID="DataSource" runat="server" AutoGenerateColumns="False" AllowPaging="True" PageSize="7" OnRowUpdating="MainGridView_OnRowUpdating" DataKeyNames="ID">
                <AlternatingRowStyle BackColor="#33CCFF"></AlternatingRowStyle>
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="false" />
                    <asp:TemplateField HeaderText="Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="NameEditTextBox" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="NameAddTextBox" runat="server" />
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="NameAddTextBox" Text="Enter name!" ValidationGroup="addGroup"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                        <HeaderStyle Width="300px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Price">
                        <EditItemTemplate>
                            <asp:TextBox ID="PriceEditTextBox" runat="server" Text='<%# Bind("Price") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="PriceLabel" runat="server" Text='<%# Bind("Price") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="PriceAddTextBox" runat="server" />
                        </FooterTemplate>
                        <HeaderStyle Width="300px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Image Name">
                        <EditItemTemplate>
                            <asp:FileUpload ID="ImageNameFileUpload" runat="server"/>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ImageNameLabel" runat="server" Text='<%# Bind("ImageName") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:FileUpload ID="ImageNameAddFileUpload" runat="server" />
                        </FooterTemplate>
                        <HeaderStyle Width="300px" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("ImageFullPath") %>' Width="100" Height="100"/>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-Width="130px">
                        <EditItemTemplate>
                            <asp:Button ID="ButtonUpdate" runat="server" CommandName="Update" Text="Update" />
                            <asp:Button ID="ButtonCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Button ID="ButtonEdit" runat="server" CommandName="Edit" Text="Edit" />
                            <asp:Button ID="ButtonDelete" runat="server" CommandName="Delete" Text="Delete" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" ValidationGroup="addGroup" OnClick="ButtonAdd_Click" />
                        </FooterTemplate>
                        <HeaderStyle Width="130px"></HeaderStyle>
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#0066FF"></HeaderStyle>
            </asp:GridView>

            <asp:ObjectDataSource
                ID="DataSource"
                runat="server"
                TypeName="TaskWeb09_AJAX_UpdatePanel_WebServices.ProductsWebService" 
                SelectMethod="GetProducts"
                DeleteMethod="DeleteProduct"
                UpdateMethod="UpdateProduct"
                OnInserting="DataSource_Inserting"
                InsertMethod="AddProduct">
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
                <UpdateParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Price" Type="String" />
                    <asp:Parameter Name="ImageName" Type="String" />
                </UpdateParameters>
                <InsertParameters>
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Price" Type="Double" />
                    <asp:Parameter Name="ImageName" Type="String" />
                </InsertParameters>
            </asp:ObjectDataSource>
        </div>
    </form>
</body>
</html>

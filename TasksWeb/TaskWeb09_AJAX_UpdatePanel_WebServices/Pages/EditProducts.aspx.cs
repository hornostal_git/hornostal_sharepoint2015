using System;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaskWeb09_AJAX_UpdatePanel_WebServices.Pages
{
    public partial class ProductsTable : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //List<Models.Product> products = new List<Models.Product>
            //{
            //    new Models.Product {ID=0,Name="Laptop X345",Price=15145.44,ImageName="LaptopX345.jpg" },
            //    new Models.Product {ID=1,Name="Laptop Z741",Price=9835.29,ImageName="LaptopZ741.jpg" },
            //    new Models.Product {ID=2,Name="Laptop AG41",Price=12175.32,ImageName="LaptopAG41.jpg" },
            //    new Models.Product {ID=3,Name="Laptop LZ5",Price=11274.71,ImageName="LaptopLZ5.jpg" }
            //};
            //XmlSerializer xml = new XmlSerializer(typeof(List<Models.Product>));
            //using (FileStream fs = new FileStream(Server.MapPath("~/1.xml"), FileMode.OpenOrCreate))
            //{
            //    xml.Serialize(fs, products);
            //}
            Models.Repository.PathToFile = Server.MapPath("~/Resources/Products.xml");
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            DataSource.Insert();
        }

        protected void DataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters["Name"] = ((TextBox)MainGridView.FooterRow.FindControl("NameAddTextBox")).Text;
            try
            {
                e.InputParameters["Price"] =
                    double.Parse(((TextBox) MainGridView.FooterRow.FindControl("PriceAddTextBox")).Text);
            }
            catch
            {
                e.InputParameters["Price"] = 0.0;
            }
            FileUpload fileUpload = (FileUpload) MainGridView.FooterRow.FindControl("ImageNameAddFileUpload");
            if (fileUpload.HasFile)
            {
                e.InputParameters["ImageName"] = fileUpload.FileName;
            }
            else
            {
                e.InputParameters["ImageName"] = "empty.jpg";
            }
        }

        protected void MainGridView_OnRowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            FileUpload fileUpload = ((FileUpload)MainGridView.Rows[e.RowIndex].FindControl("ImageNameFileUpload"));
            if (fileUpload.HasFile)
            {
                fileUpload.SaveAs(Server.MapPath("~/Resources/Images/" + fileUpload.FileName));
                DataSource.UpdateParameters["ImageName"].DefaultValue = fileUpload.FileName;
            }
            else
            {
                DataSource.UpdateParameters["ImageName"].DefaultValue = string.Empty;
            }
        }
    }
}
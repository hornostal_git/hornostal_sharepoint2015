﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Services;
using TaskWeb09_AJAX_UpdatePanel_WebServices.Models;

namespace TaskWeb09_AJAX_UpdatePanel_WebServices
{
    /// <summary>
    /// Summary description for ProductsWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ProductsWebService : WebService
    {
        string ConnectionString => ConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString;

        [WebMethod]
        public List<Product> GetProducts()
        {
            List<Product> products = new List<Product>();
            SqlConnection con = new SqlConnection(ConnectionString);
            string sql = "select * from Products";
            con.Open();
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Product product = new Product
                {
                    ID = Convert.ToInt32(dr["ID"]),
                    Name = dr["Name"].ToString(),
                    Price = double.Parse(dr["Price"].ToString()),
                    ImageName = dr["ImageName"].ToString(),
                    ImageFullPath = "~/Resources/Images/" + dr["ImageName"]
                };
                products.Add(product);
            }
            return products;
        }

        [WebMethod]
        public void DeleteProduct(int ID)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("DELETE FROM Products WHERE ID = '" + ID + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        [WebMethod]
        public void AddProduct(string Name, double Price, string ImageName)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand(@"INSERT INTO Products (ID,Name,Price,ImageName) VALUES (@ID,@Name,@Price,@ImageName)", con))
                    {
                        command.Parameters.Add("@ID", SqlDbType.Int);
                        command.Parameters["@ID"].Value = GetProducts().Max(p => p.ID) + 1;
                        command.Parameters.Add("@Name", SqlDbType.VarChar);
                        command.Parameters["@Name"].Value = Name;
                        command.Parameters.Add("@Price", SqlDbType.Real);
                        command.Parameters["@Price"].Value = Price;
                        command.Parameters.Add("@ImageName", SqlDbType.Text);
                        command.Parameters["@ImageName"].Value = ImageName;
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        [WebMethod]
        public void UpdateProduct(int ID, string Name, string Price, string ImageName)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(ConnectionString))
                {
                    con.Open();
                    string editImageNameText = ImageName != null ? ", ImageName = @ImageName" : "";
                    using (SqlCommand command = new SqlCommand(@"UPDATE Products Set Name=@Name, Price=@Price"+ editImageNameText+" WHERE ID=@ID", con))
                    {
                        command.Parameters.Add("@ID", SqlDbType.Int);
                        command.Parameters["@ID"].Value = ID;
                        command.Parameters.Add("@Name", SqlDbType.VarChar);
                        command.Parameters["@Name"].Value = Name;
                        command.Parameters.Add("@Price", SqlDbType.Real);
                        command.Parameters["@Price"].Value = Price;
                        if(ImageName!=null)
                        {
                            command.Parameters.Add("@ImageName", SqlDbType.Text);
                            command.Parameters["@ImageName"].Value = ImageName;
                        }
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

    }
}

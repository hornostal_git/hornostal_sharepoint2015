y=1;

// Регулярное выражение для логина: минимум 5 символов, начинается с большой буквы, заканчивается цифрой
patternLogin = /^[A-Z]\w{3,}[0-9]$/

// Регулярное выражение для пароля: минимум 8 символов, присутсвуют символы трех категорий из числа следующих четырех: 
//  - прописные буквы английского алфавита от A до Z;
//  - строчные буквы английского алфавита от a до z;
//  - десятичные цифры (от 0 до 9);
//  - неалфавитные символы (например, !, $, #, %)
patternPass = /(?=.{8,})((?=.*\d)(?=.*[a-z])(?=.*[A-Z])|(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_])|(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_])).*/

// Регулярное выражения для ФИО: три слова с большой буквы кирилицей церез пробел
patternFullName = /^[А-Я][а-яА-Я]+ [А-Я][а-яА-Я]+ [А-Я][а-яА-Я]+$/

// Регулярное выражение для даты в формате дд/мм/гггг
patternDate = /^\d{2}\/\d{2}\/\d{4}$/

// Регулярное выражение для почтового индекса: 5 цифр
patternIndex = /^\d{5}$/

// Регулярное выражение для электронной почты
patternEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i


function next() {
	var isValid = true
	switch(y){
		case 1:
		//isValid = isValidAuthenticationForm();
		break;
		case 2:
		isValid = isValidPersonalInfoForm();
		break;
		case 3:
		isValid = isValidAddressForm();
		break;
		case 4:
		isValid = isValidSubscriptionForm();
		break;
	}
	if(isValid) {
		$("#f"+y).hide();
		y++;
		$("#f"+y).show();
	}
	if(y==5) {
		fullResultTable();		
	}
}

function prev() {   
	$("#f"+y).hide();
	y--;
	$("#f"+y).show();
}

function getAge() {
	var today = new Date();
	var text = $("#birthDate").val();
	var birthDate = getDate(text);
	var ageDifMs = Date.now() - birthDate.getTime();
    var ageDate = new Date(ageDifMs);
    var age =  Math.abs(ageDate.getUTCFullYear() - 1970);
	if(!isNaN(age) && new Date()>birthDate) {
		$("#labelAge").html(age);
	}
	else {
		$("#labelAge").html("");
	}
}

function getDate(line) {
	var parts = line.split("/");
	var date =  new Date(parseInt(parts[2], 10),
                  parseInt(parts[1], 10) - 1,
                  parseInt(parts[0], 10));	
	return date;
}

function isValidAuthenticationForm() {
	var isValid = true;
	var firstPass = $("#passFirst").val();
	
	removeError("login");	
	if(!patternLogin.test($("#login").val())) {
		isValid = false;	
		showError("login","Введите корректный логин от 5 символов, начинающийся с большой буквы и заканчивающийся цифрой");		
	}
	
	removeError("passFirst");
	if(firstPass=="") {
		isValid = false;
		showError("passFirst","Поле не должно быть пустым");		
	}
	
	removeError("passFirst");
	if(!patternPass.test(firstPass)) {
		isValid = false;
		showError("passFirst","Пароль: мин. 8 символов, символы 3-х из след. категорий: буквы A-Z, буквы a-z, цифры 0-9, неалфавитные символы");		
	}
	
	removeError("passSecond");
	if(!(firstPass == $("#passSecond").val())) {
		isValid = false;
		showError("passSecond","Пароли должны совпадать");
	}
	
	return isValid;
}

function isValidPersonalInfoForm() {
	var isValid = true;
		
	removeError("fullName");
	if(!patternFullName.test($("#fullName").val())) {
		isValid = false;	
		showError("fullName","Введите корректные данные (3 слова с большой буквы чарез пробел)");		
	}	
	removeError("birthDate");
	if(!patternDate.test($("#birthDate").val())) {
		isValid = false;	
		showError("birthDate","Введите корректную дату");		
	}
	else if(new Date()<getDate($("#birthDate").val())) {
		isValid = false;	
		showError("birthDate","Дата рождения не может быть больше текущей даты");	
	}
    else if(parseInt($("#labelAge").html()) < 12){
		isValid = false;
		showError("birthDate","Регистрация только с 12");	
	}
	
	return isValid;
}

function isValidAddressForm() {
	var isValid = true;
	
	removeError("address");
	if(!$.trim($("#address").val())) {
		isValid = false;	
		showError("address","Введите адрес");		
	}
		
	removeError("city");
	if(!$.trim($("#city").val())) {
		isValid = false;	
		showError("city","Введите город");		
	}
	
	removeError("index");
	if(!patternIndex.test($("#index").val())) {
		isValid = false;	
		showError("index","Индекс должен содержать 5 цифр");		
	}
	
	return isValid;
}

function isValidSubscriptionForm() {
	var isValid = true;
	
	removeError("email");
	if(!patternEmail.test($("#email").val())) {
		isValid = false;	
		showError("email","Введите корректный email");		
	}
	
	if($("#subscribeCheckbox").prop("checked")) {
		removeError("checkBoxPolitics");
		if($('input:checkbox.subscribe:checked').length == 0) {
			isValid = false;	
			showError("checkBoxPolitics","Выберите хотя бы одну из подписок");			
		}
	}
	return isValid;
}

function showError(inputElementName,errorMessage) {
	$("#"+inputElementName).parent().append("<span class='error-message' style='color:red'>"+errorMessage+"</span>");
}

function removeError(inputElementName) {
	$("#"+inputElementName).parent().find(".error-message").remove();
}

function subscribe() {
	if($("#subscribeCheckbox").prop("checked")) {
		$("#subscribtionList").show();
	}
	else {
		removeError("checkBoxPolitics");
		$("#subscribtionList").hide();
	}
}

function fullResultTable() {
	$("#submitButton").show();
	
	$("#loginResult").val($("#login").val());
	$("#fullNameResult").val($("#fullName").val());
	$("#birthDateResult").val($("#birthDate").val());	
	$("#addressResult").val($("#address").val());	
	$("#cityResult").val($("#city").val());
	$("#regionResult").val($("#region option:selected").text());
	$("#indexResult").val($("#index").val());
	$("#emailResult").val($("#email").val());	
	
	if($("#subscribeCheckbox").prop("checked")) {
		$("#subscriptions").show();
		var subscriptionList = "";
		$('input:checkbox.subscribe:checked').each(function () {
			subscriptionList += ($(this).val() + " ");
		});		
		$("#subscriptionsResult").val(subscriptionList);
	}
	else {
		$("#subscriptions").hide();
	}
}

function submitInfo() {
	if(isValidAuthenticationForm() && isValidPersonalInfoForm() && isValidAddressForm() && isValidSubscriptionForm()) {
		alert("Регистрация прошла успешно!");
		$("#myForm")[0].reset();
		$("#submitButton").hide();
		$("#f"+y).hide();
		y=1;
		$("#f"+y).show();
	}
}
y=1;

// Регулярное выражение для логина: минимум 5 символов, начинается с большой буквы, заканчивается цифрой
patternLogin = /^[A-Z]\w{3,}[0-9]$/

// Регулярное выражение для пароля: минимум 8 символов, присутсвуют символы трех категорий из числа следующих четырех: 
//  - прописные буквы английского алфавита от A до Z;
//  - строчные буквы английского алфавита от a до z;
//  - десятичные цифры (от 0 до 9);
//  - неалфавитные символы (например, !, $, #, %)
patternPass = /(?=.{8,})((?=.*\d)(?=.*[a-z])(?=.*[A-Z])|(?=.*\d)(?=.*[a-zA-Z])(?=.*[\W_])|(?=.*[a-z])(?=.*[A-Z])(?=.*[\W_])).*/

// Регулярное выражения для ФИО: три слова с большой буквы кирилицей церез пробел
patternFullName = /^[А-Я][а-яА-Я]+ [А-Я][а-яА-Я]+ [А-Я][а-яА-Я]+$/

// Регулярное выражение для даты в формате дд/мм/гггг
patternDate = /^\d{2}\/\d{2}\/\d{4}$/

// Регулярное выражение для почтового индекса: 5 цифр
patternIndex = /^\d{5}$/

// Регулярное выражение для электронной почты
patternEmail = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i


function next() {
	var isValid = true
	switch(y){
		case 1:
		isValid = isValidAuthenticationForm();
		break;
		case 2:
		isValid = isValidPersonalInfoForm();
		break;
		case 3:
		isValid = isValidAddressForm();
		break;
		case 4:
		isValid = isValidSubscriptionForm();
		break;
	}
	if(isValid) {
		var currentElement = document.getElementById("f"+y);
		currentElement.style.display="none";
		y++;
		var nextElement = document.getElementById("f"+y);
		nextElement.style.display="block";
	}
	if(y==5) {
		fullResultTable();		
	}
}

function prev() {   
	var currentElement = document.getElementById("f"+y);
	currentElement.style.display="none";
	y--;
	var nextElement = document.getElementById("f"+y);
	nextElement.style.display="block";	
}

function getAge() {
	var today = new Date();
	var text = document.getElementById("birthDate").value;
	var birthDate = getDate(text);
	var ageDifMs = Date.now() - birthDate.getTime();
    var ageDate = new Date(ageDifMs);
    var age =  Math.abs(ageDate.getUTCFullYear() - 1970);
	if(!isNaN(age) && new Date()>birthDate) {
		document.getElementById('labelAge').innerHTML = age;
	}
	else {
		document.getElementById('labelAge').innerHTML = "";
	}
}

function getDate(line) {
	var parts = line.split("/");
	var date =  new Date(parseInt(parts[2], 10),
                  parseInt(parts[1], 10) - 1,
                  parseInt(parts[0], 10));	
	return date;
}

function isValidAuthenticationForm() {
	var isValid = true;
	var login = document.getElementById("login").value;
	var firstPass = document.getElementById("passFirst").value;
	var secondPass = document.getElementById("passSecond").value;
	
	removeError(document.getElementById("login"));
	if(!patternLogin.test(login)) {
		isValid = false;	
		showError(document.getElementById("login"),"Введите корректный логин от 5 символов, начинающийся с большой буквы и заканчивающийся цифрой");		
	}
	
	removeError(document.getElementById("passFirst"));
	if(firstPass=="") {
		isValid = false;
		showError(document.getElementById("passFirst"),"Поле не должно быть пустым");		
	}
	
	removeError(document.getElementById("passFirst"));
	if(!patternPass.test(firstPass)) {
		isValid = false;
		showError(document.getElementById("passFirst"),"Пароль: мин. 8 символов, символы 3-х из след. категорий: буквы A-Z, буквы a-z, цифры 0-9, неалфавитные символы");		
	}
	
	removeError(document.getElementById("passSecond"));
	if(!(firstPass == secondPass)) {
		isValid = false;
		showError(document.getElementById("passSecond"),"Пароли должны совпадать");
	}
	return isValid;
}

function isValidPersonalInfoForm() {
	var isValid = true;
	var fullName = document.getElementById("fullName").value;
	var birthDate = document.getElementById("birthDate").value;
		
	removeError(document.getElementById("fullName"));
	if(!patternFullName.test(fullName)) {
		isValid = false;	
		showError(document.getElementById("fullName"),"Введите корректные данные (3 слова с большой буквы чарез пробел)");		
	}
	
	removeError(document.getElementById("birthDate"));
	if(!patternDate.test(birthDate)) {
		isValid = false;	
		showError(document.getElementById("birthDate"),"Введите корректную дату");		
	}
	else if(new Date()<getDate(birthDate)) {
		isValid = false;	
		showError("birthDate","Дата рождения не может быть больше текущей даты");	
	}
    else if(parseInt(document.getElementById("birthDate").innerHTML) < 12){
		isValid = false;
		showError(document.getElementById("birthDate"),"Регистрация только с 12");	
	}
	return isValid;
}

function isValidAddressForm() {
	var isValid = true;
	var address = document.getElementById("address").value;
	var city = document.getElementById("address").value;
	var index = document.getElementById("index").value;
	
	removeError(document.getElementById("address"));
	if(isEmptyOrSpaces(address)) {
		isValid = false;	
		showError(document.getElementById("address"),"Введите адрес");		
	}
		
	removeError(document.getElementById("city"));
	if(isEmptyOrSpaces(city)) {
		isValid = false;	
		showError(document.getElementById("city"),"Введите город");		
	}
	
	removeError(document.getElementById("index"));
	if(!patternIndex.test(index)) {
		isValid = false;	
		showError(document.getElementById("index"),"индекс должен содержать 5 цифр");		
	}
	return isValid;
}

function isValidSubscriptionForm() {
	var isValid = true;
	var email = document.getElementById("email").value;
	
	removeError(document.getElementById("email"));
	if(!patternEmail.test(email)) {
		isValid = false;	
		showError(document.getElementById("email"),"Введите корректный email");		
	}
	
	if(document.getElementById("subscribeCheckbox").checked) {
		removeError(document.getElementById("checkBoxPolitics"));
		if(!document.getElementById("checkBoxPolitics").checked && !document.getElementById("checkBoxMusic").checked && !document.getElementById("checkBoxSport").checked) {
			isValid = false;	
			showError(document.getElementById("checkBoxPolitics"),"Выберите хотя бы одну из подписок");			
		}
	}
	return isValid;
}

function showError(inputElement,errorMessage) {
	var msgElem = document.createElement('span');
	msgElem.className = "error-message";
	var t = document.createTextNode(errorMessage);
	msgElem.style.color = "red";
	msgElem.appendChild(t); 
	inputElement.parentNode.appendChild(msgElem);
}

function removeError(inputElement) {
	if(inputElement.parentNode.lastChild.className=="error-message") {
		inputElement.parentNode.removeChild(inputElement.parentNode.lastChild);		
	}
}

function subscribe() {
	if(document.getElementById("subscribeCheckbox").checked) {
		subscribtionList.style.display = "block";
	}
	else {
		removeError(document.getElementById("checkBoxPolitics"));
		subscribtionList.style.display = "none";
	}
}

function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}

function fullResultTable() {
	document.getElementById("submitButton").style.display = "block";
	
	var login = document.getElementById("login");
	var loginResult = document.getElementById("loginResult");
	loginResult.value = login.value;
		
	var fullName = document.getElementById("fullName");
	var fullNameResult = document.getElementById("fullNameResult");
	fullNameResult.value = fullName.value;
		
	var birthDate = document.getElementById("birthDate");
	var birthDateResult = document.getElementById("birthDateResult");
	birthDateResult.value = birthDate.value;
	
	var address = document.getElementById("address");
	var addressResult = document.getElementById("addressResult");
	addressResult.value = address.value;
	
	var city = document.getElementById("city");
	var cityResult = document.getElementById("cityResult");
	cityResult.value = city.value;
	
	var region = document.getElementById("region");
	var regionResult = document.getElementById("regionResult");
	regionResult.value = region[region.selectedIndex].text;
	
	var index = document.getElementById("index");
	var indexResult = document.getElementById("indexResult");
	indexResult.value = index.value;
	
	var email = document.getElementById("email");
	var emailResult = document.getElementById("emailResult");
	emailResult.value = email.value;
	
	if(document.getElementById("subscribeCheckbox").checked) {
		document.getElementById("subscriptions").style.display = "";
		document.getElementById("subscriptionsResult").value = "";
		if(document.getElementById("checkBoxPolitics").checked)
			document.getElementById("subscriptionsResult").value += "Политика ";
		if(document.getElementById("checkBoxMusic").checked)
			document.getElementById("subscriptionsResult").value += "Музыка ";
		if(document.getElementById("checkBoxSport").checked)
			document.getElementById("subscriptionsResult").value += "Спорт ";
	}
	else {
		document.getElementById("subscriptions").style.display = "none";
	}
}

function submitInfo() {
	if(isValidAuthenticationForm() && isValidPersonalInfoForm() && isValidAddressForm() && isValidSubscriptionForm()) {
		alert("Регистрация прошла успешно");
		document.getElementById("myForm").reset();
		document.getElementById("submitButton").style.display = "none";
		
		var currentElement = document.getElementById("f"+y);
		currentElement.style.display="none";
		y=1;
		var nextElement = document.getElementById("f"+y);
		nextElement.style.display="block";	
	}
}
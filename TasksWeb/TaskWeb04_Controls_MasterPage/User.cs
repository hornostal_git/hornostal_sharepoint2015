﻿/**
 * @author Алексей Горносталь
 */
using System.ComponentModel.DataAnnotations;

namespace TaskWeb04_Controls_MasterPage
{
    class User
    {
        [Required]
        public string Nick { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string BirthDate { get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public string City { get; set; }
    }
}

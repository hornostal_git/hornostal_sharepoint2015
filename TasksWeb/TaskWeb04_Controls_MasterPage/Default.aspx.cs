﻿/**
 * @author Алексей Горносталь
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaskWeb04_Controls_MasterPage
{
    public partial class Default : System.Web.UI.Page
    {
        #region Информационные поля 

        // Список зарегистрированных пользователей.
        static List<User> _users = new List<User>();

        // Словарь стран и их доступных городов.
        static Dictionary<string, string[]> _countriesAndCities = new Dictionary<string, string[]> {
            { "Украина",new string[] { "Харьков","Киев","Днепропетровск"} },
            { "Россия", new string[] { "Москва", "Санкт-Петербург", "Новосибирск" } },
            { "Белоруссия", new string[] { "Минск", "Гомель", "Витебск" } } };

        #endregion


        #region Основные Controls

        TextBox _nickTextBox;
        TextBox _firstNameTextBox;
        TextBox _lastNameTextBox;
        TextBox _birthDate;
        Calendar _calendar;
        TextBox _email;
        MultiView _multiView;
        DropDownList _countries;
        DropDownList _cities;
        Button _submitButton;
        Button _resetButton;
        Table _table;

        #endregion


        #region Валидационные Controls

        ValidationSummary _validationSummary;
        RequiredFieldValidator _requiredFDNickName;
        RequiredFieldValidator _requiredFDFirstName;
        RequiredFieldValidator _requiredFDLastName;
        RequiredFieldValidator _requiredFDBirthDate;
        RequiredFieldValidator _requiredFDEmail;
        CustomValidator _firstNameValidator;
        CustomValidator _lastNameValidator;
        CustomValidator _birthDateValidator;
        RegularExpressionValidator _emailRegularExpressionValidator;

        #endregion


        #region Labels

        Label _nickLabel;
        Label _firstNameLabel;
        Label _lastNameLabel;
        Label _birthDateLabel;
        Label _emailLabel;
        Label _countriesLabel;
        Label _citiesLabel;

        #endregion


        protected void Page_PreInit(object sender, EventArgs e)
        {
            InitializeMainControls();
            InitializeVailidationControls();
            InitializeLabels();
            CreateMultiView();
        }


        #region Инициализаторы компонентов страницы

        // Выполняет инициализацию основных Controls.
        private void InitializeMainControls()
        {
            _nickTextBox = new TextBox { ID = "nick" };

            _firstNameTextBox = new TextBox { ID = "firstName" };

            _lastNameTextBox = new TextBox { ID = "lastName" };

            _birthDate = new TextBox { ID = "birthDate", ReadOnly = true };

            _calendar = new Calendar();

            _calendar.SelectionChanged += CalendarOnSelectionChanged;

            _email = new TextBox { ID = "email" };

            _submitButton = new Button { Text = "Зарегистрироваться!" };
            _submitButton.Click += submitButton_Click;

            _resetButton = new Button { Text = "Очистить форму" };
            _resetButton.Click += resetButton_Click;

            _multiView = new MultiView();

            _countries = new DropDownList() { AutoPostBack = true, ID = "country", Width = 147 };
            _countries.SelectedIndexChanged += CountriesSelectedIndexChanged;
            foreach (var key in _countriesAndCities.Keys)
            {
                _countries.Items.Add(new ListItem(key));
            }

            _cities = new DropDownList { ID = "city", Width = 147 };
            string[] cities = _countriesAndCities[_countriesAndCities.Keys.First()];
            foreach (var city in cities)
            {
                _cities.Items.Add(new ListItem(city));
            }

            _table = new Table { BorderWidth = 4, GridLines = GridLines.Both };
        }

        // Выполняет инициализацию валидационных Controls.
        private void InitializeVailidationControls()
        {
            _validationSummary = new ValidationSummary
            {
                ID = "validationSummary",
                ShowModelStateErrors = true,
                ShowSummary = true
            };

            _requiredFDNickName = new RequiredFieldValidator
            {
                ControlToValidate = "nick",
                ErrorMessage = "Введите псевдоним!",
                Text = "*"
            };

            _requiredFDFirstName = new RequiredFieldValidator
            {
                ControlToValidate = "firstName",
                ErrorMessage = "Введите имя!",
                Text = "*"
            };

            _requiredFDLastName = new RequiredFieldValidator
            {
                ControlToValidate = "lastName",
                ErrorMessage = "Введите фамилию!",
                Text = "*"
            };

            _requiredFDBirthDate = new RequiredFieldValidator
            {
                ControlToValidate = "birthDate",
                ErrorMessage = "Выберите дату рождения!",
                Text = "*"
            };

            _requiredFDEmail = new RequiredFieldValidator
            {
                ControlToValidate = "email",
                ErrorMessage = "Введите email!",
                Text = "*"
            };

            _firstNameValidator = new CustomValidator
            {
                ErrorMessage = "Имя должно быть отлично от псевдонима.",
                ControlToValidate = "firstName",
                Text = "*"
            };
            _firstNameValidator.ServerValidate += FirstNameValidator;

            _lastNameValidator = new CustomValidator
            {
                ErrorMessage = "Фамилия должна быть отличной от имени.",
                ControlToValidate = "lastName",
                Text = "*"
            };
            _lastNameValidator.ServerValidate += LastNameValidator;

            _birthDateValidator = new CustomValidator
            {
                ErrorMessage = "Дата рождения должна быть в диапозоне от 01.12.1960 до сегодняшнего дня!",
                ControlToValidate = "birthDate",
                Text = "*"
            };
            _birthDateValidator.ServerValidate += BirthDateValidator;

            _emailRegularExpressionValidator = new RegularExpressionValidator
            {
                ErrorMessage = "Введите корректный email!",
                ControlToValidate = "email",
                Text = "*",
                ValidationExpression = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            };
        }

        // Выполняет инициализацию Labels.
        private void InitializeLabels()
        {
            _nickLabel = new Label { Text = "Псевдоним", Width = 120 };
            _firstNameLabel = new Label { Text = "Имя", Width = 120 };
            _lastNameLabel = new Label { Text = "Фамилия", Width = 120 };
            _birthDateLabel = new Label { Text = "Дата рождения", Width = 120 };
            _emailLabel = new Label { Text = "Email", Width = 120 };
            _countriesLabel = new Label { Text = "Страна", Width = 120 };
            _citiesLabel = new Label { Text = "Город", Width = 120 };
        }

        // Создаёт основной MultiView страницы, заполняет его компонентами и добавляет на страницу.
        private void CreateMultiView()
        {
            View view1 = new View();

            view1.Controls.Add(_validationSummary);

            // Псевдониим
            view1.Controls.Add(_nickLabel);
            view1.Controls.Add(_nickTextBox);
            view1.Controls.Add(_requiredFDNickName);

            view1.Controls.Add(new LiteralControl("<br />"));

            // Имя
            view1.Controls.Add(_firstNameLabel);
            view1.Controls.Add(_firstNameTextBox);
            view1.Controls.Add(_requiredFDFirstName);
            view1.Controls.Add(_firstNameValidator);

            view1.Controls.Add(new LiteralControl("<br />"));

            // Фамилия
            view1.Controls.Add(_lastNameLabel);
            view1.Controls.Add(_lastNameTextBox);
            view1.Controls.Add(_requiredFDLastName);
            view1.Controls.Add(_lastNameValidator);

            view1.Controls.Add(new LiteralControl("<br />"));

            // Дата рождения
            view1.Controls.Add(_birthDateLabel);
            view1.Controls.Add(_birthDate);
            view1.Controls.Add(_requiredFDBirthDate);
            view1.Controls.Add(_birthDateValidator);
            view1.Controls.Add(_calendar);

            view1.Controls.Add(new LiteralControl("<br />"));

            // Электронная почта
            view1.Controls.Add(_emailLabel);
            view1.Controls.Add(_email);
            view1.Controls.Add(_requiredFDEmail);
            view1.Controls.Add(_emailRegularExpressionValidator);

            view1.Controls.Add(new LiteralControl("<br />"));

            // Страна
            view1.Controls.Add(_countriesLabel);
            view1.Controls.Add(_countries);

            view1.Controls.Add(new LiteralControl("<br />"));

            // Город
            view1.Controls.Add(_citiesLabel);
            view1.Controls.Add(_cities);

            view1.Controls.Add(new LiteralControl("<br />"));

            view1.Controls.Add(_submitButton);
            view1.Controls.Add(_resetButton);

            _multiView.Views.Add(view1);
            _multiView.ActiveViewIndex = 0;
            View view2 = new View();
            view2.Controls.Add(new Label { Text = "<b>Вы успешно зарегистрированы!</b><br/>Уже успели зарегистрироваться:" });
            view2.Controls.Add(_table);
            _multiView.Views.Add(view2);
            form1.Controls.Add(_multiView);
        }

        #endregion


        #region Методы валидации

        // Проверяет, помещается ли дата в диапозон между 01.12.1960 и текущим днём.
        private void BirthDateValidator(object source, ServerValidateEventArgs args)
        {
            args.IsValid = DateTime.Parse(_birthDate.Text) > DateTime.Parse("01.12.1960") && DateTime.Parse(_birthDate.Text) <= DateTime.Now;
        }

        // Проверяет, отличается ли имя от псевдонима.
        private void FirstNameValidator(object source, ServerValidateEventArgs args)
        {
            args.IsValid = _nickTextBox.Text != _firstNameTextBox.Text;
        }

        // Проверяет, отличается ли фамилия от имени.
        private void LastNameValidator(object source, ServerValidateEventArgs args)
        {
            args.IsValid = _firstNameTextBox.Text != _lastNameTextBox.Text;
        }

        #endregion


        #region Обработчики пользовательских событий страницы

        // Событие при выборе страны (отображает соответствующий список городов).
        private void CountriesSelectedIndexChanged(object sender, EventArgs e)
        {
            _cities.Items.Clear();
            string[] cities = _countriesAndCities[_countries.SelectedItem.Text];
            foreach (var city in cities)
            {
                _cities.Items.Add(new ListItem(city));
            }
        }

        // Событие при выборе даты рождения (отображает дату в текстовом поле).
        private void CalendarOnSelectionChanged(object sender, EventArgs e)
        {
            _birthDate.Text = _calendar.SelectedDate.Date.ToShortDateString();
        }

        // Обработчик для кнопки Submit
        private void submitButton_Click(object sender, EventArgs eventArgs)
        {
            Page.Validate();
            if (Page.IsValid)
            {
                User newUser = new User();
                IValueProvider provider =
                    new FormValueProvider(ModelBindingExecutionContext);
                if (TryUpdateModel(newUser, provider))
                {
                    _users.Add(newUser);
                }

                foreach (var user in _users)
                {
                    TableRow r = new TableRow();

                    TableCell c = new TableCell();
                    c.Controls.Add(new LiteralControl(user.Nick));
                    r.Cells.Add(c);

                    c = new TableCell();
                    c.Controls.Add(new LiteralControl(user.FirstName));
                    r.Cells.Add(c);

                    c = new TableCell();
                    c.Controls.Add(new LiteralControl(user.LastName));
                    r.Cells.Add(c);

                    c = new TableCell();
                    c.Controls.Add(new LiteralControl(user.BirthDate));
                    r.Cells.Add(c);

                    c = new TableCell();
                    c.Controls.Add(new LiteralControl(user.Email));
                    r.Cells.Add(c);

                    c = new TableCell();
                    c.Controls.Add(new LiteralControl(user.Country));
                    r.Cells.Add(c);

                    c = new TableCell();
                    c.Controls.Add(new LiteralControl(user.City));
                    r.Cells.Add(c);

                    _table.Rows.Add(r);

                }
                _multiView.ActiveViewIndex = 1;
            }
        }

        // Обработчик для кнопки Reset
        private void resetButton_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx", true);
        }

        #endregion
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TaskWeb10_Membership.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Button Text="Показать всех пользователей" OnClick="ShowUsers" runat="server"/>
        <asp:Button Text="Больше информации о выбранном пользователе" OnClick="ShowMoreInfo" runat="server"/>
    </div>
    </form>
</body>
</html>

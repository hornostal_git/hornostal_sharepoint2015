﻿using System;
using System.Drawing;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaskWeb10_Membership
{
    public partial class Default : Page
    {
        private Panel _usersPanel;
        DropDownList _dropDownList;
        Panel _moreInfoPanel;
        Label _lblName;
        TextBox _txtEmail;
        TextBox _txtPassword;
        TextBox _txtConfirmPassword;
        private Label _infoLabel;

        ValidationSummary _validationSummary;
        private RequiredFieldValidator _requiredFDName;
        private RequiredFieldValidator _requiredFDEmail;
        private RegularExpressionValidator _emailRegularExpressionValidator;
        private RegularExpressionValidator _passwordRegularExpressionValidator;
        private CompareValidator _passwordConfirmValidaotr;
        private CheckBox _cancelPasswordRecoveryCheckBox;

        protected void Page_Load(object sender, EventArgs e)
        {
            //Membership.CreateUser("admin", "adminADMIN2015", "alexa@gmail.com");
            //Membership.CreateUser("AlexA", "alexAX%%1", "alexa@gmail.com");
            //Membership.CreateUser("AlexB", "alexBX%%1", "alexb@gmail.com");
            //Membership.CreateUser("AlexC", "alexCX%%1", "alexc@gmail.com");
            //Membership.CreateUser("AlexD", "alexDX%%1", "alexd@gmail.com");

            if (!IsPostBack)
            {
                Session["ShowUsers"] = false;
                Session["ShowUserInfo"] = false;
                Session["CurrentUser"] = "";
            }
            if ((bool)Session["ShowUsers"])
            {
                InitializeShowUsersPanel();
                form1.Controls.Add(_usersPanel);
            }
            if ((bool)Session["ShowUserInfo"])
            {
                InitializeMoreInfoPanel();
                form1.Controls.Add(_moreInfoPanel);
            }
        }

        protected void ShowUsers(object sender, EventArgs e)
        {
            Session["ShowUsers"] = true;
            Session["ShowUserInfo"] = false;
            form1.Controls.Remove(_moreInfoPanel);
            if (form1.Controls.Contains(_usersPanel))
            {
                return;
            }
            InitializeShowUsersPanel();
            form1.Controls.Add(_usersPanel);
        }

        #region Методы обработки событий

        private void DeleteButton_Click(object sender, EventArgs e)
        {
            if (_dropDownList.SelectedItem == null) return;
            Membership.DeleteUser(_dropDownList.SelectedItem.Text);
            _dropDownList.Items.RemoveAt(_dropDownList.SelectedIndex);
        }

        protected void ShowMoreInfo(object sender, EventArgs e)
        {
            if (_dropDownList?.SelectedItem == null) return;
            Session["ShowUsers"] = false;
            Session["ShowUserInfo"] = true;
            form1.Controls.Remove(_usersPanel);
            Session["CurrentUser"] = _dropDownList.SelectedItem.Text;
            if (form1.Controls.Contains(_moreInfoPanel))
            {
                return;
            }
            InitializeMoreInfoPanel();
            form1.Controls.Add(_moreInfoPanel);
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                MembershipUser user = Membership.GetUser((string)Session["CurrentUser"]);
                if (_cancelPasswordRecoveryCheckBox.Checked == false)
                {
                    if (_txtPassword.Text.Length > 5)
                    {
                        if (_txtPassword.Text == _txtConfirmPassword.Text)
                        {
                            user?.ChangePassword(user.ResetPassword(), _txtPassword.Text);
                        }
                        else
                        {
                            _infoLabel.Text = "Пароли должны совпадать!";
                            return;
                        }
                    }
                    else
                    {
                        _infoLabel.Text = "Пароль должен быть не меньше 6 символов!";
                        return;
                    }
                }
                user.Email = _txtEmail.Text;
                Membership.UpdateUser(user);
                Session["ShowUserInfo"] = false;
                form1.Controls.Remove(_moreInfoPanel);
            }
        }

        #endregion


        #region Инициализаторы контролов

        // Выполняет инициализацию валидационных Controls.
        private void InitializeVailidationControls()
        {
            _validationSummary = new ValidationSummary
            {
                ID = "validationSummary",
                ShowModelStateErrors = true,
                ShowSummary = true
            };

            _requiredFDEmail = new RequiredFieldValidator
            {
                ControlToValidate = "emailTextBox",
                ErrorMessage = "Введите email!",
                Text = "*"
            };

            _passwordConfirmValidaotr = new CompareValidator
            {
                ErrorMessage = "Пароли должны совпадать.",
                ControlToValidate = "confirmPasswordTextBox",
                ControlToCompare = "passwordTextBox",
                Text = "*"
            };

            _emailRegularExpressionValidator = new RegularExpressionValidator
            {
                ErrorMessage = "Введите корректный email!",
                ControlToValidate = "emailTextBox",
                Text = "*",
                ValidationExpression = @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
            };

            _passwordRegularExpressionValidator = new RegularExpressionValidator
            {
                ErrorMessage = "Введите пароль не меньше 6 символов!",
                ControlToValidate = "passwordTextBox",
                Text = "*",
                ValidationExpression = @"^.{6,}$"
            };
        }

        // Выполняет инициализацию панели с выпадающим списком пользователей.
        private void InitializeShowUsersPanel()
        {
            _usersPanel = new Panel { ID = "mainUsersPanel" };
            _dropDownList = new DropDownList { ID = "dropDownUsers" };
            foreach (ListItem item in (from MembershipUser user in Membership.GetAllUsers() select new ListItem { Text = user.UserName }).Where(item => item.Text != "admin"))
            {
                _dropDownList.Items.Add(item);
            }
            _usersPanel.Controls.Add(_dropDownList);
            Button deleteButton = new Button { Text = "Delete" };
            deleteButton.Click += DeleteButton_Click;
            _usersPanel.Controls.Add(deleteButton);
        }

        // Выполняет инициализацию панели с дополнительной информацией.
        private void InitializeMoreInfoPanel()
        {
            MembershipUser user = Membership.GetUser((string)Session["CurrentUser"]);
            _lblName = new Label { Text = user?.UserName, ID = "nameTextBox" };
            _txtEmail = new TextBox { Text = user?.Email, ID = "emailTextBox" };
            _txtPassword = new TextBox { ID = "passwordTextBox", TextMode = TextBoxMode.Password };
            _txtConfirmPassword = new TextBox { ID = "confirmPasswordTextBox", TextMode = TextBoxMode.Password };
            _cancelPasswordRecoveryCheckBox = new CheckBox { ID = "cancelPasswordRecoveryCheckBox", Text = "Не менять пароль" };
            _infoLabel = new Label { ID = "infoLabel", ForeColor = Color.Red };
            _moreInfoPanel = new Panel { ID = "mainPanel" };


            _moreInfoPanel.Controls.Add(new Label { Text = "Иия пользователя", Width = 250 });
            _moreInfoPanel.Controls.Add(_lblName);
            _moreInfoPanel.Controls.Add(new LiteralControl("<br/>"));
            _moreInfoPanel.Controls.Add(new Label { Text = "E-mail", Width = 250 });
            _moreInfoPanel.Controls.Add(_txtEmail);
            _moreInfoPanel.Controls.Add(new LiteralControl("<br/>"));
            _moreInfoPanel.Controls.Add(_cancelPasswordRecoveryCheckBox);
            _moreInfoPanel.Controls.Add(new LiteralControl("<br/>"));
            _moreInfoPanel.Controls.Add(new Label { Text = "Пароль", Width = 250 });
            _moreInfoPanel.Controls.Add(_txtPassword);
            _moreInfoPanel.Controls.Add(new LiteralControl("<br/>"));
            _moreInfoPanel.Controls.Add(new Label { Text = "Подтверждение пароля", Width = 250 });
            _moreInfoPanel.Controls.Add(_txtConfirmPassword);
            _moreInfoPanel.Controls.Add(new LiteralControl("<br/>"));

            InitializeVailidationControls();
            _moreInfoPanel.Controls.Add(_validationSummary);
            _moreInfoPanel.Controls.Add(_requiredFDEmail);
            _moreInfoPanel.Controls.Add(_emailRegularExpressionValidator);
            _moreInfoPanel.Controls.Add(_passwordConfirmValidaotr);
            _moreInfoPanel.Controls.Add(_passwordRegularExpressionValidator);

            Button updateButton = new Button { Text = "Обновить", ID = "updateButton" };
            updateButton.Click += UpdateButton_Click;
            _moreInfoPanel.Controls.Add(updateButton);
            _moreInfoPanel.Controls.Add(new LiteralControl("<br/>"));
            _moreInfoPanel.Controls.Add(_infoLabel);
        }

        #endregion
    }
}
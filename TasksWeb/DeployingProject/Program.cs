﻿/**
 * @author Алексей Горносталь
 */
using Microsoft.Web.Administration;

namespace DeployingProject
{
    class Program
    {
        static void Main(string[] args)
        {
            string pathToDirectoryWithSite;
            do
            {
                System.Console.WriteLine("Введите путь к директории с сайтом и нажмите Enter.");
                System.Console.WriteLine("Для вставки нажмите в левом углу на иконку. Далее выберите Изменить -> Вставить.");
                pathToDirectoryWithSite = System.Console.ReadLine();
            } while (string.IsNullOrWhiteSpace(pathToDirectoryWithSite) && !System.IO.File.Exists(pathToDirectoryWithSite));

            // Определение названия сайта по директории, в которой он размещён.
            int lastIndexOfSeparator = pathToDirectoryWithSite.LastIndexOf("\\");
            if (lastIndexOfSeparator == -1)
                lastIndexOfSeparator = pathToDirectoryWithSite.LastIndexOf("/");
            lastIndexOfSeparator++;
            string siteName = pathToDirectoryWithSite.Substring(lastIndexOfSeparator, pathToDirectoryWithSite.Length - lastIndexOfSeparator);
            System.Console.WriteLine(siteName);
            System.Console.ReadKey();

            ServerManager serverManager = new ServerManager();
            Site mySite = serverManager.Sites.Add(siteName, pathToDirectoryWithSite, 8080);
            mySite.ServerAutoStart = true;
            serverManager.CommitChanges();

            Site site = serverManager.Sites[siteName];
            site.Applications[0].VirtualDirectories[0].PhysicalPath = pathToDirectoryWithSite;
            serverManager.ApplicationPools.Add(siteName + "Pool");
            serverManager.Sites[siteName].Applications[0].ApplicationPoolName = siteName + "Pool";
            ApplicationPool apppool = serverManager.ApplicationPools[siteName + "Pool"];
            serverManager.CommitChanges();
        }
    }
}
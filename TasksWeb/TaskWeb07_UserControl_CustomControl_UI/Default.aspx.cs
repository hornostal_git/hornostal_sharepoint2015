﻿using TaskWeb07_UserControl_CustomControl_Dll;
using System;
using System.Collections.Generic;

namespace TaskWeb07_UserControl_CustomControl_UI
{
    public partial class Default : System.Web.UI.Page
    {
        private static List<Question> _questions;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                _questions = new List<Question>
                {
                    new Question
                    {
                        Title = "2+2=",
                        Answers = new List<Answer>() {new Answer("4"), new Answer("5"), new Answer("6")},
                        CorrectAnswerIndex = 0
                    },
                    new Question
                    {
                        Title = "3+3=",
                        Answers = new List<Answer>() {new Answer("4"), new Answer("5"), new Answer("6")},
                        CorrectAnswerIndex = 2
                    },
                    new Question
                    {
                        Title = "4+4=",
                        Answers = new List<Answer>() {new Answer("7"), new Answer("8"), new Answer("9")},
                        CorrectAnswerIndex = 1
                    }
                };
            }
            QuestionServerControl.Questions = _questions;
            QuestionServerControl.TestEnded += QuestionServerControl_TestEnded;
            QuestionServerControl.QuestionAnswered += QuestionServerControl_QuestionAnswered;
        }

        private void QuestionServerControl_QuestionAnswered(object sender, EventArgs e)
        {
            Response.Write("Получен ответ на вопрос " + ((Question)e).Title);
        }

        private void QuestionServerControl_TestEnded(object sender, EventArgs e)
        {
            foreach (var answer in QuestionServerControl.Questions)
            {
                Response.Write($"{answer.Title} {answer.Answers.Find(a => a.IsSelected).Content}<br/>");
            }
        }
    }
}
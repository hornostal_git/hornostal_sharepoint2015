﻿/**
 * @author Алексей Горносталь
 */
using System;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace TaskWeb05_HTTPModule_HTTPHandler
{
    // Огрнаичивает доступ к страницам, если в адресной строке не передан зашифрованый ключ
    class AuthorizationModule : IHttpModule
    {
        // Пароль в незашифрованом виде
        const string Password = "I_Am_Real_Admin";

        public void Init(HttpApplication context)
        {
            context.PostAcquireRequestState += Application_PostAcquireRequestState;
            context.PostMapRequestHandler += Application_PostMapRequestHandler;
        }

        void Application_PostMapRequestHandler(object source, EventArgs e)
        {
            HttpApplication app = (HttpApplication)source;

            if (app.Context.Handler is IReadOnlySessionState || app.Context.Handler is IRequiresSessionState)
            {
                // Нет необходимости изменять текущий обработчик
                return;
            }

            // Заменяет текущий обработчик
            app.Context.Handler = new MyHttpHandler(app.Context.Handler);
        }

        // Проверяет при каждом запросе пользовательский ключ
        void Application_PostAcquireRequestState(object source, EventArgs e)
        {
            HttpApplication app = (HttpApplication)source;

            MyHttpHandler resourceHttpHandler = HttpContext.Current.Handler as MyHttpHandler;
            if (resourceHttpHandler != null)
            {
                // Возвращает на место прежний обработчик
                HttpContext.Current.Handler = resourceHttpHandler.OriginalHandler;
            }

            string key = app.Request.QueryString["UserKey"];
            using (MD5 md5Hash = MD5.Create())
            {
                if (CheckMd5Hash(md5Hash, key))
                {
                    app.Context.Session["isAuthorized"] = true;
                }
            }
            try
            {
                if (app.Context.Session?["isAuthorized"] == null || !(bool)app.Session["isAuthorized"])
                {
                    app.Response.Clear();
                    app.Response.Write("<html><body>You are not authorized");
                    app.Response.End();
                }
            }
            catch
            {
                // ignored
            }
        }

        // Возвращает MD5 хэш
        static string GetMd5Hash(MD5 md5Hash, string input)
        {
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sBuilder = new StringBuilder();
            foreach (byte t in data)
            {
                sBuilder.Append(t.ToString("x2"));
            }
            return sBuilder.ToString();
        }

        // Проверяет соотвествует ли введённый ключ пользователя необходимому
        static bool CheckMd5Hash(MD5 md5Hash, string key)
        {
            return (!string.IsNullOrWhiteSpace(key) && key.Equals(GetMd5Hash(md5Hash, Password)));
        }

        public void Dispose()
        {

        }

        // Временный обработчик, который заставляет SessionStateModule загрузить session state
        private class MyHttpHandler : IHttpHandler, IRequiresSessionState
        {
            internal readonly IHttpHandler OriginalHandler;

            public MyHttpHandler(IHttpHandler originalHandler)
            {
                OriginalHandler = originalHandler;
            }

            public void ProcessRequest(HttpContext context)
            {
                throw new InvalidOperationException("MyHttpHandler cannot process requests.");
            }

            public bool IsReusable => false;
        }
    }
}

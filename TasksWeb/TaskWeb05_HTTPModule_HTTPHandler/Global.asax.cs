﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TaskWeb05_HTTPModule_HTTPHandler
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            var ext = new List<string> { ".jpg", ".gif", ".png" };
            var myFiles = Directory.GetFiles(Server.MapPath("~") + "Images\\", "*.*", SearchOption.AllDirectories)
                 .Where(s => ext.Any(s.EndsWith));
            foreach (var file in myFiles)
            {
                if (file.Contains("tempImageWithWatermark"))
                {
                    continue;
                }
                ImageRepository.RegisterImage(file.Substring(file.LastIndexOf("\\", StringComparison.Ordinal) + 1, file.Length - file.LastIndexOf("\\", StringComparison.Ordinal) - 1));
            }
        }
    }
}
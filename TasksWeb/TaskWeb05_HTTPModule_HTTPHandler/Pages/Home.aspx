﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="TaskWeb05_HTTPModule_HTTPHandler.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="ImagesDropDownList" runat="server"></asp:DropDownList>
            <asp:Button ID="ButtonDownload" runat="server" Text="Download" OnClick="DownloadButton_Click" />
            <br />
            <asp:HyperLink runat="server" NavigateUrl="~/Pages/Upload.aspx">Upload Page</asp:HyperLink>
        </div>
    </form>
</body>
</html>

﻿/**
 * @author Алексей Горносталь
 */
using System;

namespace TaskWeb05_HTTPModule_HTTPHandler
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            foreach (var line in ImageRepository.EnumerateIdPathPairs())
            {
                ImagesDropDownList.Items.Add($"#{line.Key}: {line.Value}");
            }
        }

        protected void DownloadButton_Click(object sender, EventArgs e)
        {
            string selectedItemText = ImagesDropDownList.SelectedItem.ToString();
            int index = selectedItemText.IndexOf(":", StringComparison.Ordinal) + 2;
            Response.Redirect("/Images/" + selectedItemText.Substring(index)+ "?UserKey=b9e52202515e507b646867ae703c68f5", true);
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="TaskWeb05_HTTPModule_HTTPHandler.Upload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:FileUpload ID="FileUploadButton" runat="server" />
            <asp:Button ID="SubmitButton" runat="server" OnClick="SubmitButton_Click" Text="Upload" />
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidator1" runat="server"
                ErrorMessage="This is a required field!"
                ControlToValidate="FileUploadButton"></asp:RequiredFieldValidator>
            <asp:ValidationSummary runat="server" />
            <br />
            <asp:HyperLink runat="server" NavigateUrl="~/Pages/Home.aspx">Home Page</asp:HyperLink>
        </div>
    </form>
</body>
</html>

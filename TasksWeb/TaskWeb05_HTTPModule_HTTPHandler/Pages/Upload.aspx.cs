﻿/**
 * @author Алексей Горносталь
 */
using System;
using System.Linq;

namespace TaskWeb05_HTTPModule_HTTPHandler
{
    public partial class Upload : System.Web.UI.Page
    {
        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            if (FileUploadButton.HasFile)
            {
                string imageSavedPath = Server.MapPath("~") + "Images/" +
                                        FileUploadButton.FileName;
                string fileExtension = System.IO.Path.GetExtension(FileUploadButton.FileName).ToLower();
                string[] allowedExtensions = { ".gif", ".png", ".jpeg", ".jpg" };
                bool isImage = allowedExtensions.Count(ext => ext.Equals(fileExtension)) == 1;

                if (isImage)
                {
                    try
                    {
                        FileUploadButton.SaveAs(imageSavedPath);
                        Response.Write("File name: " +
                                       FileUploadButton.PostedFile.FileName + "<br>" +
                                       FileUploadButton.PostedFile.ContentLength + " kb<br>" +
                                       "Content type: " +
                                       FileUploadButton.PostedFile.ContentType);
                        ImageRepository.RegisterImage(FileUploadButton.FileName);
                        Response.Write("ID of this file is " + ImageRepository.LastId);
                    }
                    catch (Exception ex)
                    {
                        Response.Write("ERROR: " + ex.Message);
                    }
                }
                else
                {
                    {
                        Response.Write("Неправильное расширение файла!");
                    }
                }
            }
            else
            {
                Response.Write("Выберите файл!");
            }
        }
    }
}
﻿/**
 * @author Алексей Горносталь
 */
using System.Collections.Generic;

namespace TaskWeb05_HTTPModule_HTTPHandler
{
    // Хранит методы работы с изображениями в веб-приложении
    static class ImageRepository
    {
        // Идентификатор изображений
        private static int _id;

        // Последний идентификатор
        public static int LastId => _id;

        // Новый (уникальный) идентификатор
        private static int GenerateId => _id++;

        // Хранит пары "идентификатор изображения / имя файла"
        private static readonly Dictionary<int, string> Images = new Dictionary<int, string>();

        // Добавляет изображение в словарь, генерируя новый идентификатор
        public static void RegisterImage(string path)
        {
            Images.Add(GenerateId, path);
        }

        // Возвращает коллекцию пар "идентификатор изображения / имя файла"
        public static IEnumerable<KeyValuePair<int, string>> EnumerateIdPathPairs()
        {
            return Images;
        }

        // Позволяет получить имя файла изображения по идентификатору
        public static string GetImageNameById(int id)
        {
            if (id < 0 || id >= LastId)
            {
                return null;
            }
            string fileName;
            Images.TryGetValue(id, out fileName);
            return fileName;
        }
    }
}
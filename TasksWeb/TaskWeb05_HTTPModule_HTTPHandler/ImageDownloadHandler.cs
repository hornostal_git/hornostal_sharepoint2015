﻿/**
 * @author Алексей Горносталь
 */
using System.Drawing;
using System.IO;
using System.Web;

namespace TaskWeb05_HTTPModule_HTTPHandler
{
    // Обрабатывает обращение к изображению, позволяя скачать его с водяным знаком
    class ImageDownloadHandler : IHttpHandler
    {
        public bool IsReusable => true;

        // Добавляет к картинке необходимый текст и скачивает её пользователю
        public void ProcessRequest(HttpContext context)
        {
            HttpResponse response = context.Response;
            HttpRequest request = context.Request;

            string watermarkText = System.Configuration.ConfigurationManager.AppSettings["WatermarkText"];

            string pathToImages = context.Server.MapPath("~/Images/");
            FileInfo currentFile = new FileInfo(context.Server.MapPath("~" + request.Path));

            Bitmap bitmap = (Bitmap)Image.FromFile(currentFile.FullName);
            PointF firstLocation = new PointF(0,0);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                using (Font arialFont = new Font("Arial", 20))
                {
                    graphics.DrawString(watermarkText, arialFont, Brushes.Chartreuse, firstLocation);
                }
            }

            bitmap.Save(pathToImages+"tempImageWithWatermark."+ currentFile.Extension);

            response.Clear();
            response.AddHeader("Content-Disposition", "attachment; filename=" + context.Server.MapPath("~/Images/temp.jpg"));
            response.ContentType = "image/" + currentFile.Extension;
            response.TransmitFile(pathToImages+"tempImageWithWatermark." + currentFile.Extension);
            response.End();
        }
    }
}

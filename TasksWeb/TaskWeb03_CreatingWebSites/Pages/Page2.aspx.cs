﻿/**
 * @author Алексей Горносталь
 */
namespace TaskWeb03_CreatingWebSites
{
    public partial class Page2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {
            string currentPageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath);
            int count = (int)Application[currentPageName];
            count++;
            Application[currentPageName] = count;
        }
    }
}
﻿/**
 * @author Алексей Горносталь
 */
using System;
using System.Collections.Generic;

namespace TaskWeb03_CreatingWebSites
{
    // Содержит поля и методы, используемые при ведении статистики web-приложения.
    static class Statistics
    {
        // Счётчик запросов к web-приложению (за всё время).
        public static int RequestsAll
        {
            get
            {
                return (int)Global.CurrentApplication["RequestsAll"];
            }
            set
            {
                Global.CurrentApplication["RequestsAll"] = value;
            }
        }

        // Счётчик запросов к web-приложению (за день).
        public static int RequestsPerDay
        {
            get
            {
                if (((DateTime)Global.CurrentApplication["RequestsPerDayDate"]) == DateTime.Now.Date)
                    return (int)Global.CurrentApplication["RequestsPerDay"];
                else
                {
                    Global.CurrentApplication["RequestsPerDayDate"] = DateTime.Now.Date;
                    Global.CurrentApplication["RequestsPerDay"] = 1;
                    return 1;
                }
            }
            set
            {
                Global.CurrentApplication["RequestsPerDay"] = value;
            }
        }

        // Счётчик посетителей (за день).
        public static int VisitorsPerDay
        {
            get
            {
                if (((DateTime)Global.CurrentApplication["VisitorsPerDayDate"]) == DateTime.Now.Date)
                    return (int)Global.CurrentApplication["VisitorsPerDay"];
                else
                {
                    Global.CurrentApplication["VisitorsPerDayDate"] = DateTime.Now.Date;
                    Global.CurrentApplication["VisitorsPerDay"] = 0;
                    return 0;
                }
            }
            set
            {
                Global.CurrentApplication["VisitorsPerDay"] = value;
            }
        }

        // Счётчик уникальных посетителей (за всё время).
        public static int UniqVisitorsCount { get { return _visitors.Count; } }

        // Добавляет посетителя в список, если у него отличается браузер или ip от уже существубщих в списке
        public static void RegisterVisitor(string browser, string ip)
        {
            Visitor visitor = new Visitor { Browser = browser, Ip = ip };
            if (_visitors.Find(v => v.Browser.Equals(visitor.Browser) && v.Ip.Equals(visitor.Ip)) != null)
                return;
            else
            {
                _visitors.Add(visitor);
            }
        }

        // Посетитель
        private class Visitor
        {
            // Бпаузер посетителя
            public string Browser;

            // IP-адрес посетителя
            public string Ip;
        }

        // Список уникальных посетителей
        private static List<Visitor> _visitors = new List<Visitor>();
    }
}

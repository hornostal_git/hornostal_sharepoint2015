﻿/**
 * @author Алексей Горносталь
 */
namespace TaskWeb03_CreatingWebSites
{
    public partial class StatisticsPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, System.EventArgs e)
        {

            foreach (string page in (string[])Application["Pages"])
            {
                links.InnerHtml += string.Format("<br/><a href='{0}'>{0}</a>", page);
            }

            labelInfo.Text += "1. Количество запросов к web-приложению (за все время): " + Statistics.RequestsAll + "<br/>";

            labelInfo.Text += "2. Количество запросов к web-приложению (за день): " + Statistics.RequestsPerDay + "<br/>";

            labelInfo.Text += "3. Количество запросов к конкретной странице (за все время): <br/>";
            labelInfo.Text += "<pre>";
            foreach (string page in (string[])Application["Pages"])
            {
                labelInfo.Text += "&#9;Количество посещений страницы " + page + ": " + Application[page] + "<br/>";
            }
            labelInfo.Text += "</pre>";

            labelInfo.Text += "4. Количество уникальных посетителей (за все время): " + Statistics.UniqVisitorsCount + "<br/>";

            labelInfo.Text += "5. Количество посетителей (за день): " + Statistics.VisitorsPerDay + "<br/>";
        }
    }
}
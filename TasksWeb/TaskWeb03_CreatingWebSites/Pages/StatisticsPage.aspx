﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StatisticsPage.aspx.cs" Inherits="TaskWeb03_CreatingWebSites.StatisticsPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Статистика</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h1>Статистика</h1>
        <p>Добро пожаловать на страницу статистики!</p>
        <asp:Label ID="labelInfo" runat="server"></asp:Label>
        <br />
        <div id="links" runat="server"></div>
    </div>
    </form>
</body>
</html>

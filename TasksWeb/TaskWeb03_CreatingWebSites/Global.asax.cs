﻿/**
 * @author Алексей Горносталь
 */
using System;

namespace TaskWeb03_CreatingWebSites
{
    public class Global : System.Web.HttpApplication
    {
        // Поле, открывающе доступ к Application для класса Statistics.
        public static System.Web.HttpApplicationState CurrentApplication;

        // Событие старта приложения (происхоит инициализация счётчиков).
        void Application_Start(object sender, EventArgs e)
        {
            var path = System.Web.Configuration.WebConfigurationManager.AppSettings["PagesFileName"];
            Application["Pages"] = System.IO.File.ReadAllLines(Server.MapPath(path));
            foreach (string page in (string[])Application["Pages"])
            {
                Application[page] = 0;
            }
            CurrentApplication = Application;
            Application["VisitorsPerDayDate"] = DateTime.Now.Date;
            Application["RequestsPerDayDate"] = DateTime.Now.Date;
            Statistics.VisitorsPerDay = 0;
            Statistics.RequestsAll = 0;
            Statistics.RequestsPerDay = 0;
        }

        // Событие начала запроса к web-приложению.
        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Statistics.RequestsAll++;
            Statistics.RequestsPerDay++;
        }

        // Событие начала сессии (означает приход посетителя).
        void Session_Start(object sender, EventArgs e)
        {
            Statistics.VisitorsPerDay++;
            Statistics.RegisterVisitor(Request.Browser.Browser, Request.UserHostAddress);
        }
    }
}
﻿
using System;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace DatabaseRestoreUtility
{
    static class Program
    {
        static void Main()
        {
            Console.WriteLine("Введите имя сервера:");
            string serverName = Console.ReadLine();
            Console.WriteLine("Введите имя пользователя:");
            string userName = Console.ReadLine();
            Console.WriteLine("Введите пароль:");
            string password = Console.ReadLine();
            Console.WriteLine("Введите имя базы данных:");
            string databaseName = Console.ReadLine();
            Console.WriteLine("Введите имя файла БД или полный путь к нему, если он лежит в другой папке:");
            string backUpFile = Console.ReadLine();

            ServerConnection connection = new ServerConnection(serverName, userName, password);
            Server sqlServer = new Server(connection);
            Restore rstDatabase = new Restore
            {
                Action = RestoreActionType.Database,
                Database = databaseName,
                ReplaceDatabase = true
            };
            BackupDeviceItem bkpDevice = new BackupDeviceItem(backUpFile, DeviceType.File);
            rstDatabase.Devices.Add(bkpDevice);
            rstDatabase.SqlRestore(sqlServer);
        }
    }
}

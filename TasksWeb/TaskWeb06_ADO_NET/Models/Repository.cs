﻿/**
 * @author Алексей Горносталь
 */
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace TaskWeb06_ADO_NET.Models
{
    public class Repository
    {
        public static string SelectedFileFullPath = "";

        public IEnumerable<Record> GetRecords()
        {
            if (string.IsNullOrWhiteSpace(SelectedFileFullPath))
                return null;
            return from emp in XDocument.Load(SelectedFileFullPath).Descendants("data")
                   select new Record
                   {
                       ID = emp.Attribute("name").Value,
                       Name = emp.Attribute("name").Value,
                       Value = emp.Element("value").Value,
                       Comment = emp.OptionalElement("comment")
                   };
        }

        public IEnumerable<Record> GetRecords(int startRowIndex, int maximumRows)
        {
            return GetRecords().Skip(startRowIndex).Take(maximumRows);
        }

        public int GetRecordsAmount()
        {
            return GetRecords().Count();
        }        

        public void DeleteRecord(string ID)
        {
            XDocument doc = XDocument.Load(SelectedFileFullPath);
            doc.Root?.Elements("data")
                .Where(l => l.Attribute("name").Value.Equals(ID))
                .Remove();
            doc.Save(SelectedFileFullPath);
        }
        
        public void AddRecord(string Name, string Value, string Comment)
        {
            XDocument doc = XDocument.Load(SelectedFileFullPath);
            doc.Root.Add(
            new XElement("data",
                        new XAttribute("name", Name),
                        new XElement("value", Value),
                        new XElement("comment", Comment)
                        ));
            doc.Save(SelectedFileFullPath);
        }

        public void UpdateRecord(string ID, string Name, string Value, string Comment)
        {
            XDocument doc = XDocument.Load(SelectedFileFullPath);
            var el = doc.Root.Elements("data");
            var el2 = el.First(l => l.Attribute("name").Value.Equals(ID));
            el2.Element("value").Value = Value;
            el2.Attribute("name").Value = Name;
            if (string.IsNullOrWhiteSpace(Comment) && el2.Element("comment") == null || (el2.Element("comment") == null && string.IsNullOrWhiteSpace(el2.Element("comment").Value)))
            {
                doc.Save(SelectedFileFullPath);
                return;
            }
            if (string.IsNullOrWhiteSpace(Comment))
            {                
                Comment = string.Empty;
            }
            if (el2.Element("comment") != null)
            {
                el2.Element("comment").Value = Comment;
            }
            else
            {
                el2.Add(new XElement("comment", Comment));
            }
            doc.Save(SelectedFileFullPath);            
        }
    }
}
﻿
/**
 * @author Алексей Горносталь
 */
namespace TaskWeb06_ADO_NET.Models
{
    public class Record
    {
        // ИД, который получается из имени
        public string ID { get; set; }

        // Имя записи
        public string Name { get; set; }

        // Значение записи
        public string Value { get; set; }

        //Комментарий
        public string Comment { get; set; }

        public override string ToString()
        {
            return Name + " " + Value;
        }   
    }
}

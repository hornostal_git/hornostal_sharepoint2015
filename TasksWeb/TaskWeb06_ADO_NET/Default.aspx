﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="TaskWeb06_ADO_NET.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <p>Выберите из списка необходимый файл для просмотра/редактирования:</p>
            <asp:DropDownList ID="MainDropDownList" runat="server" OnSelectedIndexChanged="MainDropDownList_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            <br />
            <asp:Label ID="FullPathLabel" runat="server"></asp:Label>
            <br />
            Ошибки:<asp:Label ID="ErrorsLabel" runat="server" ForeColor="Red"></asp:Label>
            <br />
            <asp:GridView HeaderStyle-BackColor="#0066ff" AlternatingRowStyle-BackColor="#33ccff" ID="MainGridView" EnableViewState="False" ShowFooter="True"  DataSourceID="DataSource" runat="server" AutoGenerateColumns="False" PageSize="20" AllowPaging="True" DataKeyNames="ID">
                            <AlternatingRowStyle BackColor="#33CCFF"></AlternatingRowStyle>
                <Columns>
                    <asp:BoundField DataField="ID" HeaderText="ID" Visible="false"/>
                    <asp:TemplateField HeaderText="Name">
                        <EditItemTemplate>
                            <asp:TextBox ID="NameEditTextBox" runat="server" Text='<%# Bind("Name") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="NameLabel" runat="server" Text='<%# Bind("Name") %>'></asp:Label>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID = "NameAddTextBox" runat="server" />                          
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="NameAddTextBox" Text="Enter name!" ValidationGroup="addGroup"></asp:RequiredFieldValidator>
                        </FooterTemplate>
                        <HeaderStyle Width="300px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Value">
                        <EditItemTemplate>
                            <asp:TextBox ID="ValueEditTextBox" runat="server" Text='<%# Bind("Value") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="ValueLabel" runat="server" Text='<%# Bind("Value") %>'></asp:Label>
                        </ItemTemplate>
                         <FooterTemplate>
                            <asp:TextBox ID = "ValueAddTextBox" runat="server" />
                        </FooterTemplate>
                        <HeaderStyle Width="300px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Comment">
                        <EditItemTemplate>
                            <asp:TextBox ID="CommentEditTextBox" runat="server" Text='<%# Bind("Comment") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="CommentLabel" runat="server" Text='<%# Bind("Comment") %>'></asp:Label>
                        </ItemTemplate>
                         <FooterTemplate>
                            <asp:TextBox ID = "CommentAddTextBox" runat="server" />
                        </FooterTemplate>
                        <HeaderStyle Width="300px" />
                    </asp:TemplateField>
                   <asp:TemplateField HeaderStyle-Width="130px">
                    <EditItemTemplate>
                        <asp:Button ID="ButtonUpdate" runat="server" CommandName="Update" Text="Update" />
                        <asp:Button ID="ButtonCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Button ID="ButtonEdit" runat="server" CommandName="Edit" Text="Edit" />
                        <asp:Button ID="ButtonDelete" runat="server" CommandName="Delete" Text="Delete" />
                    </ItemTemplate>
                    <FooterTemplate>
                        <asp:Button ID="ButtonAdd" runat="server" Text="Add New Row" OnClick="ButtonAdd_Click" ValidationGroup="addGroup"/>
                    </FooterTemplate>
                <HeaderStyle Width="130px"></HeaderStyle>
                </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#0066FF"></HeaderStyle>
            </asp:GridView>

            <asp:ObjectDataSource OnInserting="DataSource_Inserting"
                ID="DataSource"
                runat="server"
                EnablePaging="true"
                TypeName="TaskWeb06_ADO_NET.Models.Repository"
                SelectMethod="GetRecords"
                SelectCountMethod="GetRecordsAmount"
                DeleteMethod="DeleteRecord"
                InsertMethod="AddRecord"
                UpdateMethod="UpdateRecord">
                <UpdateParameters>
                    <asp:Parameter Name="ID" Type="String" />
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Value" Type="String" />
                    <asp:Parameter Name="Comment" Type="String" />
                </UpdateParameters>
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="String" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="Name" Type="String" />
                    <asp:Parameter Name="Value" Type="String" />
                    <asp:Parameter Name="Comment" Type="String" />
                </InsertParameters>
            </asp:ObjectDataSource>
        </div>
    </form>
</body>
</html>

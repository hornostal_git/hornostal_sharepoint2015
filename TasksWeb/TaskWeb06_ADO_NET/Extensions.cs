﻿/**
 * @author Алексей Горносталь
 */
using System.Xml.Linq;

namespace TaskWeb06_ADO_NET
{
    static class Extensions
    {
        // Позволяет безопасно взять комментарий в запросе Linq-to-Xml
        public static string OptionalElement(this XElement actionElement, string elementName)
        {
            var element = actionElement.Element(elementName);
            return element?.Value;
        }
    }
}

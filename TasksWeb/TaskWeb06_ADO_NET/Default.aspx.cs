﻿/**
 * @author Алексей Горносталь
 */
using System;
using System.Configuration;
using System.Linq;
using System.Web.UI.WebControls;
using TaskWeb06_ADO_NET.Models;

namespace TaskWeb06_ADO_NET
{
    public partial class Default : System.Web.UI.Page
    {
        static string _selectedFileFullPath = "";
        string _pathToWorkingDirectory;
        static int _selectedIndex;

        protected void Page_Load(object sender, EventArgs e)
        {
            _pathToWorkingDirectory = ConfigurationManager.AppSettings["PathToWorkingDirectory"];
            if (IsPostBack)
            {
                _selectedIndex = MainDropDownList.SelectedIndex;
                _selectedFileFullPath = Server.MapPath(_pathToWorkingDirectory + MainDropDownList.SelectedItem);
            }
            else
            {
                MainDropDownList.Items.Clear();

                var files = GetAllFilesInDirectory();
                if (files.Length == 0)
                {
                    ErrorsLabel.Text = @"Файлы с соответсвующим расширением не были найдены в рабочей дирректории!";
                    return;
                }
                ErrorsLabel.Text = "";
                foreach (var file in files)
                {
                    MainDropDownList.Items.Add(file.Substring(file.LastIndexOf("\\", StringComparison.Ordinal) + 1, file.Length - file.LastIndexOf("\\", StringComparison.Ordinal) - 1));
                }
                MainDropDownList.SelectedIndex = _selectedIndex;
                _selectedFileFullPath = Server.MapPath(_pathToWorkingDirectory + MainDropDownList.Items[0]);
                FullPathLabel.Text = _selectedFileFullPath;
                Repository.SelectedFileFullPath = _selectedFileFullPath;
            }
        }

        protected void MainDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            _selectedFileFullPath = Server.MapPath(_pathToWorkingDirectory + MainDropDownList.SelectedItem);
            FullPathLabel.Text = _selectedFileFullPath;
            Repository.SelectedFileFullPath = _selectedFileFullPath;
            MainGridView.DataBind();
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            DataSource.Insert();
        }

        protected void DataSource_Inserting(object sender, ObjectDataSourceMethodEventArgs e)
        {
            e.InputParameters["Name"] = ((TextBox)MainGridView.FooterRow.FindControl("NameAddTextBox")).Text;
            e.InputParameters["Value"] = ((TextBox)MainGridView.FooterRow.FindControl("ValueAddTextBox")).Text;
            e.InputParameters["Comment"] = ((TextBox)MainGridView.FooterRow.FindControl("CommentAddTextBox")).Text;
        }

        private string[] GetAllFilesInDirectory()
        {
            return System.IO.Directory.GetFiles(Server.MapPath(_pathToWorkingDirectory))
                 .Where(s => s.EndsWith(".resx")).ToArray();
        }
    }
}
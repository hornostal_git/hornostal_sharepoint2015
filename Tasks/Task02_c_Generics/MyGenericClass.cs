﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;
using System.Collections.Generic;

/// <summary>
/// Содержит решение задания по теме "Циклы, коллекции, Generic типы, Generic коллекции" b: Коллекции.
/// </summary>
namespace Task02_c_Generics
{
    /// <summary>
    /// Разработанный обобщённый класс с обощённым автоматическим свойством.
    /// </summary>
    /// <typeparam name="T"> Тип вутреннего обобщённого свойства </typeparam>
    public class MyGenericClass<T>: IComparable<MyGenericClass<T>> 
        where T : struct, IComparable<T>
    {
        /// <summary>
        /// Внутреннее обобщённое свойство.
        /// </summary>
        public T MyGenericProperty { get; set; }

        public int CompareTo(MyGenericClass<T> other)
        {
            return MyGenericProperty.CompareTo(other.MyGenericProperty);
        }
    }

    /// <summary>
    /// Класс, описывающий способ сравнения обобщённых объектов
    /// </summary>
    /// <typeparam name="T"> Тип обобщённых объектов </typeparam>
    public class MyComparerClass<T> : IComparer<MyGenericClass<T>> where T : struct, IComparable<T>
    {
        /// <summary>
        /// Сравнивает два объекта.
        /// </summary>
        /// <param name="X"> Первый объект для сравнения </param>
        /// <param name="Y"> Второй объект для сравнения </param>
        /// <returns> Меньше нуля - X предшествует Y в порядке сортировки.</returns>
        /// <returns> Нуль - X занимает ту же позицию в порядке сортировки, что и Y.</returns>
        /// <returns> Больше нуля - X следует за Y в порядке сортировки.</returns>
        public int Compare(MyGenericClass<T> X, MyGenericClass<T> Y)
        {
            return X.MyGenericProperty.CompareTo(Y.MyGenericProperty);
        }
    }
}
﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;
using System.Collections.Generic;

/// <summary>
/// Содержит решение задания по теме "Циклы, коллекции, Generic типы, Generic коллекции" C: Generics.
/// </summary>
namespace Task02_c_Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            List<MyGenericClass<int>> genericsInt = new List<MyGenericClass<int>> {
                new MyGenericClass<int> { MyGenericProperty = 9 },
                new MyGenericClass<int> { MyGenericProperty = 1},
                new MyGenericClass<int> { MyGenericProperty = 7 },
                new MyGenericClass<int> { MyGenericProperty = 4 },
                new MyGenericClass<int> { MyGenericProperty = 15 }};
            genericsInt.Sort(new MyComparerClass<int>());
            foreach (var value in genericsInt)
                Console.WriteLine(value.MyGenericProperty);

            List<MyGenericClass<float>> genericsFloat = new List<MyGenericClass<float>> {
                new MyGenericClass<float> { MyGenericProperty = 0.7f },
                new MyGenericClass<float> { MyGenericProperty = 0.1f },
                new MyGenericClass<float> { MyGenericProperty = 0.9f },
                new MyGenericClass<float> { MyGenericProperty = 2.7f },
                new MyGenericClass<float> { MyGenericProperty = 1.5f }};
            genericsFloat.Sort(new MyComparerClass<float>());
            foreach (var value in genericsFloat)
                Console.WriteLine(value.MyGenericProperty);

            List<MyGenericClass<float>> genericsFloat2 = new List<MyGenericClass<float>> {
                new MyGenericClass<float> { MyGenericProperty = 0.7f },
                new MyGenericClass<float> { MyGenericProperty = 0.1f },
                new MyGenericClass<float> { MyGenericProperty = 0.9f },
                new MyGenericClass<float> { MyGenericProperty = 2.7f },
                new MyGenericClass<float> { MyGenericProperty = 1.5f }};
            genericsFloat.Sort();
            foreach (var value in genericsFloat)
                Console.WriteLine(value.MyGenericProperty);


            Console.ReadKey();
        }
    }
}

﻿/**
* @author Горносталь Алексей Андреевич
*/
using System.Text.RegularExpressions;

namespace Task06_Core
{
    /// <summary>
    /// Содержит методы валидации строк.
    /// </summary>
    public static class StringsValidator
    {
        #region Внутренние поля - регулярные выражения

        /// <summary>
        /// Регулярное выражение "только цифры".
        /// </summary>
        const string _onlyNumbersPattern = "^[0-9]+$";

        /// <summary>
        /// Регулярное выражение "только буквы с первой заглавной".
        /// </summary>
        const string _namePattern = "^[A-Z][a-z]+$";

        /// <summary>
        /// Регулярное выражение "им файла с поддерживаемым расширением".
        /// </summary>
        const string _imageFileNamePattern = @"([^\s]+(\.(?i)(jpg|png|gif|bmp|tiff))$)";

        #endregion

        #region Методы проверки строк

        /// <summary>
        /// Проверяет, может ли строка быть именем или фамилией.
        /// </summary>
        /// <param name="name"> проверяемая строка </param>
        /// <returns> true - строка может быть именем или фамилией </returns>
        public static bool IsValidName(string name)
        {
            return Regex.IsMatch(name, _namePattern);
        }

        /// <summary>
        /// Проверяет, может ли строка быть названием группы.
        /// </summary>
        /// <param name="group"> проверяемая строка </param>
        /// <returns> true - строка может быть название группы </returns>
        public static bool IsValidGroup(string group)
        {
            return !System.String.IsNullOrWhiteSpace(group);
        }

        /// <summary>
        /// Проверяет, может ли строка быть номером телефона.
        /// </summary>
        /// <param name="number"> проверяемая строка </param>
        /// <returns> true - строка может быть номером телефона </returns>
        public static bool IsValidNumber(string number)
        {
            return Regex.IsMatch(number, _onlyNumbersPattern);
        }

        /// <summary>
        /// Проверяет, может ли строка быть именем файла с изображением.
        /// </summary>
        /// <param name="imageFileName"> проверяемая строка </param>
        /// <returns> true - строка может быть именем файла с изображением </returns>
        public static bool IsValidImageFileName(string imageFileName)
        {
            return Regex.IsMatch(imageFileName, _imageFileNamePattern)
                && System.IO.File.Exists(imageFileName);
        }
        
        #endregion
    }
}

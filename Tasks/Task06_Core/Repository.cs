﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Task06_Core
{
    /// <summary>
    /// Содержит методы работы с xml-хранилищем.
    /// </summary>
    public static class Repository
    {

        static Repository()
        {            
            TestFile();
        }

        /// <summary>
        /// Имя файла, оторый является xml-хранилищем.
        /// </summary>
        private static readonly string _repositoryFileName = Path.GetFullPath(
                Strings.PathToResources +
                Strings.RepositoryFolderName +
                Strings.RepositoryFileName);

        /// <summary>
        /// Загружает xml-хранилище для дальнейшей с ним работы.
        /// </summary>
        private static XDocument _doc;

        /// <summary>
        /// Обеспечивает доступ к контактам из xml-хранилища.
        /// </summary>
        static public IEnumerable<Contact> Contacts
        {
            get
            {
                return from emp in _doc.Descendants("Contact")
                       select new Contact
                       {
                           FirstName = emp.Element("FirstName").Value,
                           LastName = emp.Element("LastName").Value,
                           Group = emp.Element("Group").Value,
                           HomeTelephoneNumber = emp.Element("HomeTelephoneNumber").Value,
                           MobileTelephoneNumber = emp.Element("MobileTelephoneNumber").Value,
                           PhotoName = emp.Element("PhotoName").Value,
                       };
            }
        }

        /// <summary>
        /// Обеспечивает удаление элементов из xml-хранилища.
        /// </summary>
        /// <param name="line"> Строка, содержащая имя и фамилию через пробел. </param>
        public static void RemoveElement(string line)
        {
            string[] lines = line.Split(' ');
            _doc.Root.Elements("Contact")
                .Elements("FirstName")
                .Where(l => l.Value == lines[0])
                .Select(x => x.Parent)
                .Elements("LastName")
                .Where(l => l.Value == lines[1])
                .Select(x => x.Parent)
                .Remove();
            _doc.Save(_repositoryFileName);
        }

        /// <summary>
        /// Добавляет элемент в xml-хранилище.
        /// </summary>
        /// <param name="contact"> Контакт, котоырй нужно добавить. </param>
        public static void AddElement(Contact contact)
        {
            _doc.Element("ArrayOfContact").Add(
            new XElement("Contact",
                        new XElement("FirstName", contact.FirstName),
                        new XElement("LastName", contact.LastName),
                        new XElement("Group", contact.Group),
                        new XElement("HomeTelephoneNumber", contact.HomeTelephoneNumber),
                        new XElement("MobileTelephoneNumber", contact.MobileTelephoneNumber),
                        new XElement("PhotoName", contact.PhotoName)));

            _doc.Save(_repositoryFileName);
        }

        /// <summary>
        /// Редактирует контакт в xml-хранилище.
        /// </summary>
        /// <param name="oldContact"> Старый контакт. </param>
        /// <param name="newContact"> Новый контакт. </param>
        public static void EditElement(Contact oldContact, Contact newContact)
        {
            var el = _doc.Root.Elements("Contact")
                .Elements("FirstName")
                .Where(l => l.Value == oldContact.FirstName)
                .Select(x => x.Parent)
                .Elements("LastName")
                .Where(l => l.Value == oldContact.LastName)
                .Select(x => x.Parent).First();
            el.Element("FirstName").Value = newContact.FirstName;
            el.Element("LastName").Value = newContact.LastName;
            el.Element("Group").Value = newContact.Group;
            el.Element("HomeTelephoneNumber").Value = newContact.HomeTelephoneNumber;
            el.Element("MobileTelephoneNumber").Value = newContact.MobileTelephoneNumber;
            el.Element("PhotoName").Value = newContact.PhotoName;
            _doc.Save(_repositoryFileName);
        }

        /// <summary>
        /// Проверяет наличие хранилища (если его нет, то создаёт новое).
        /// </summary>
        private static void TestFile()
        {
            try
            {
                _doc = XDocument.Load(_repositoryFileName);
            }
            catch
            {
                List<Contact> contacts = new List<Contact>();
                XmlSerializer serializer = new XmlSerializer(typeof(List<Contact>));
                using (FileStream fs = new FileStream(_repositoryFileName, FileMode.Create))
                {
                    serializer.Serialize(fs, contacts);
                }
                _doc = XDocument.Load(_repositoryFileName);
            }
        }
    }
}


﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System.Collections.Generic;
using System.Linq;

namespace Task06_Core
{
    /// <summary>
    /// Содержит методы работы с контактами.
    /// </summary>
    public static class ContactsUtility
    {
        /// <summary>
        /// Возвращает список с тестовыми контактами.
        /// </summary>
        //public static List<Contact> GetTestData
        //{
        //    get
        //    {
        //        return new List<Contact>()
        //        {
        //            new Contact { FirstName = "Alex", LastName="Antipov", HomeTelephoneNumber="3765965", MobileTelephoneNumber="0637756592", Group="SharePoint", PhotoName="AlexG.jpg"},
        //            new Contact { FirstName = "Denis", LastName="Fomenko", HomeTelephoneNumber="7555465", MobileTelephoneNumber="0958457592", Group="Java", PhotoName="DenisF.jpg"},
        //            new Contact { FirstName = "Mikhail", LastName="Nikitenko", HomeTelephoneNumber="3721499", MobileTelephoneNumber="0666756592", Group="SharePoint", PhotoName="MihkailN.jpg"},
        //            new Contact { FirstName = "Oleg", LastName="Kolesnikov", HomeTelephoneNumber="3747976", MobileTelephoneNumber="0993845592", Group="dotNet", PhotoName="OlegK.jpg"},
        //            new Contact { FirstName = "Maksim", LastName="Topchinskiy", HomeTelephoneNumber="3758778", MobileTelephoneNumber="0666674992", Group="dotNet", PhotoName="MaksimT.jpg"},
        //            new Contact { FirstName = "Paulo", LastName="Vorona", HomeTelephoneNumber="3787144", MobileTelephoneNumber="0666756205", Group="Java", PhotoName="PauloV.jpg"},
        //            new Contact { FirstName = "Anton", LastName="Reznik", HomeTelephoneNumber="3738541", MobileTelephoneNumber="0667536905", Group="SharePoint", PhotoName="AntonR.jpg"}
        //        };
        //    }
        //}

        /// <summary>
        /// Ищет контакт по строке, содержащей имя и фамилию через пробел.
        /// </summary>
        /// <param name="contacts"> Контакты, среди которых нужно искать. </param>
        /// <param name="lineRepresentation"> Строка с именем и фамилией через пробел. </param>
        /// <returns> Найденный контакт. </returns>
        public static Contact FindContactByLine(IEnumerable<Contact> contacts, string lineRepresentation)
        {
            string[] lines = lineRepresentation.Split(' ');
            return contacts.FirstOrDefault<Contact>(
                        c => c.FirstName == lines[0] && c.LastName == lines[1]
                        );
        }

        /// <summary>
        /// Группирует контакты по группам.
        /// </summary>
        /// <param name="contacts"> Контакты, которые нужно сгрупирвоать. </param>
        /// <returns> Группы контактов. </returns>
        public static IEnumerable<IGrouping<string, Contact>> GroupByGroup(IEnumerable<Contact> contacts)
        {
            return contacts.GroupBy(o => o.Group);
        }
    }
}

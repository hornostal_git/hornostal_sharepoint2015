﻿
namespace Task06_Core
{
    public static class Strings
    {
        public const string PathToResources = "../../Resources/";
        public const string RepositoryFolderName = "Repository/";
        public const string RepositoryFileName = "repository.xml";
        public const string PhotosFolderName = "Photos/";
        public const string DefaultImageName = "NoPhoto.jpg";
    }
}

﻿/**
 * @author Горносталь Алексей Андреевич
 */

/// <summary>
/// Содержит логику "Менеджера контактов" для решения задания по теме "Linq, лямбда выражения".
/// </summary>
namespace Task06_Core
{
    /// <summary>
    /// Контакт.
    /// </summary>
    public class Contact
    {
        /// <summary>
        /// Имя.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Фамилия.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Группа.
        /// </summary>
        public string Group { get; set; }

        /// <summary>
        /// Номер домашнего телефона.
        /// </summary>
        public string HomeTelephoneNumber { get; set; }

        /// <summary>
        /// Номер мобильного телефона.
        /// </summary>
        public string MobileTelephoneNumber { get; set; }

        /// <summary>
        /// Название изображения с фотографией.
        /// </summary>
        public string PhotoName { get; set; }

        public Contact() { }

        public Contact(string firstName, string lastName, string group, string homeTelephoneNumber, string mobileTelephoneNumber, string photoName)
        {
            FirstName = firstName;
            LastName = lastName;
            Group = group;
            HomeTelephoneNumber = homeTelephoneNumber;
            MobileTelephoneNumber = mobileTelephoneNumber;
            PhotoName = photoName;
        }
           
        /// <summary>
        /// Представляет контакт в виде строки.
        /// </summary>
        /// <returns> Строковое представление контакта. </returns>                     
        public override string ToString()
        {
            return string.Format("{0} {1}",FirstName,LastName,Group);
        }
    }
}

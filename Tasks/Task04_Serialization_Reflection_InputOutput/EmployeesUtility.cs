﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml.Serialization;

namespace Task04_Serialization_Reflection_InputOutput
{
    /// <summary>
    /// Содержит методы работы с сотрудниками.
    /// </summary>
    static class EmployeesUtility
    {
        /// <summary>
        /// Сериализатор для работы с массивами сотрудников.
        /// </summary>
        private static XmlSerializer serializer = new XmlSerializer(typeof(Employee[]), new XmlRootAttribute("Employees"));

        /// <summary>
        /// Формирует тестовый массив с данными.
        /// </summary>
        /// <returns> Тестовый массив сотрудников.  </returns>
        public static Employee[] GetTestArrayOfEmployee()
        {
            Employee[] employees = new Employee[]
                        {
                            new Employee { FirstName="Alex",LastName="Nikolenko",Age=26,Department="SharePoint",Address="Kharkiv, Elizarova str." },
                            new Employee { FirstName="Denis",LastName="Avramenko",Age=19,Department="Java",Address="Kharkiv, Sumskaya str." },
                            new Employee { FirstName="Anton",LastName="Nikiforov",Age=35,Department="dotNet",Address="Kharkiv, Tankopiya str." },
                            new Employee { FirstName="Mark",LastName="Shevchenko",Age=29,Department="SEO",Address="Kharkiv, Karazina str." },
                            new Employee { FirstName="Alina",LastName="Antipenko",Age=23,Department="SharePoint",Address="Kharkiv, Narimanova str." },
                            new Employee { FirstName="Anatoliy",LastName="Filonenko",Age=33,Department="Java",Address="Kharkiv, Poltavskiy Shlyah str." },
                            new Employee { FirstName="Tatyana",LastName="Semenova",Age=41,Department="dotNet",Address="Kharkiv, Petrovskogo str." },
                            new Employee { FirstName="Sergey",LastName="Shishkin",Age=39,Department="SEO",Address="Kharkiv, Pushkinskaya str." }
                        };
            foreach (Employee employee in employees)
            {
                employee.GenerateID();
            }
            return employees;
        }

        /// <summary>
        /// Сериализирует массив сотрудников.
        /// </summary>
        /// <param name="employees"> Массив для сериализации.</param>
        public static void SerializeEmployees(Employee[] employees,string fileName)
        {
            using (FileStream fs = new FileStream(fileName, FileMode.OpenOrCreate))
            {
                serializer.Serialize(fs, employees);
            }
        }

        /// <summary>
        /// Десериализирует массив сотрудников.
        /// </summary>
        /// <returns> Полученный из файла массив сотрудников. </returns>
        public static Employee[] DeserializeEmployees(string fileName)
        {
            Employee[] employees;

            using (FileStream fs = new FileStream(fileName, FileMode.Open))
            {
                employees = (Employee[])serializer.Deserialize(fs);
            }

            foreach (Employee employee in employees)
            {
                employee.GenerateID();
            }

            return employees;
        }

        /// <summary>
        /// Выводит на экран информацию о сотруднкиах.
        /// </summary>
        /// <param name="employees"> Список сотрудников. </param>
        public static void ShowEmployees(Employee[] employees)
        {
            int counter = 0;
            foreach (Employee employee in employees)
            {
                Console.WriteLine("#{0}:", ++counter);
                Console.WriteLine("\tLast Name\t{0}", employee.LastName);
                Console.WriteLine("\tFirst Name:\t{0}", employee.FirstName);
                Console.WriteLine("\tAge:\t\t{0}", employee.Age);
                Console.WriteLine("\tDepartment:\t{0}", employee.Department);
                Console.WriteLine("\tAddress:\t{0}", GetEmployeeID(employee, "_address"));
                Console.WriteLine("\tEmployee ID:\t{0}", GetEmployeeID(employee, "_employeeID"));
            }
            Console.ReadKey();
        }

        /// <summary>
        /// Позволяет получить приватное поле у объекта Employee.
        /// </summary>
        /// <param name="employee"> Сотрудник, у которого нужно получить приватное поле. </param>
        /// <returns> Значение приватного поля. </returns>
        public static string GetEmployeeID(Employee employee, string fieldName)
        {
            FieldInfo field = new Employee().GetType().GetField(fieldName, BindingFlags.NonPublic | BindingFlags.Instance);   
            return (string)field.GetValue(employee);
        }

        /// <summary>
        /// Ищет сотрудников в возрасте от 25 до 35 лет и сортирует их по EmployeeID.
        /// </summary>
        /// <param name="employees"> Последовательность сотрудников для поиска. </param>
        /// <returns> Последователность найденных и упорядоченных сотрудников. </returns>
        public static Employee[] GetSpecialEmployees(Employee[] employees)
        {
            Employee[] specialEmployees = (from emp in employees
                                                     where emp.Age >= 25 && emp.Age <= 35
                                                     orderby GetEmployeeID(emp, "_employeeID")
                                                     select emp).ToArray();
            return specialEmployees;
        }
    }
}

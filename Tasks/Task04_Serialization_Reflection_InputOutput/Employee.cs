﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System;

namespace Task04_Serialization_Reflection_InputOutput
{
    /// <summary>
    /// Реализует сущность "Сотрудник".
    /// </summary>
    [Serializable]
    public class Employee
    {
        /// <summary>
        /// Идентификатор сотрудника
        /// </summary>
        private string _employeeID;

        /// <summary>
        /// Адрес проживания сотрудника.
        /// </summary>
        private string _address;

        /// <summary>
        /// Фамилия сотрудника.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Возраст сотрудника.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Отдел, где работает сотрудник.
        /// </summary>
        public string Department { get; set; }

        /// <summary>
        /// Обеспечивает доступ к адресу проживания сотрудника.
        /// </summary>
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        /// <summary>
        /// Генерирует идентификатор.
        /// </summary>
        public void GenerateID()
        {
            _employeeID = LastName + FirstName;
        }
    }
}

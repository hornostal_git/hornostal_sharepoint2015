﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System;

/// <summary>
/// Содержит решение задания по теме "Сериализация, рефлексия, Input/Output".
/// </summary>
namespace Task04_Serialization_Reflection_InputOutput
{
    class Program
    {
        const string KeyToAllEmployees = "AllEmployeesFileName";
        const string KeyToSpecialEmployees = "SpecialEmployeesFileName";

        static void Main(string[] args)
        {
            string allEmployeesFileName =
                System.Configuration.ConfigurationManager.AppSettings[KeyToAllEmployees];

            string specialEmployeesFileName =
                System.Configuration.ConfigurationManager.AppSettings[KeyToSpecialEmployees];

            Employee[] employees = new Employee[0];

            string command = "";
            do
            {
                Console.Clear();
                ShowMenuText();
                command = Console.ReadLine();
                switch (command)
                {
                    case "serialize":
                        employees = EmployeesUtility.GetTestArrayOfEmployee();
                        EmployeesUtility.SerializeEmployees(employees, allEmployeesFileName);
                        break;

                    case "deserialize":
                        employees = EmployeesUtility.DeserializeEmployees(allEmployeesFileName);
                        break;

                    case "all":
                        EmployeesUtility.ShowEmployees(employees);
                        break;

                    case "special":
                        Employee[] employeeArray = EmployeesUtility.GetSpecialEmployees(employees);
                        EmployeesUtility.ShowEmployees(employeeArray);
                        EmployeesUtility.SerializeEmployees(employeeArray, specialEmployeesFileName);
                        break;

                    case "exit":
                        Console.WriteLine("Good Luck!");
                        break;

                    default:
                        Console.WriteLine("Incorrect input!");
                        break;
                }
                Console.WriteLine("Press any key to continue");
                Console.ReadKey();
            } while (command != "exit");

        }

        /// <summary>
        /// Выводит на экран текст меню.
        /// </summary>
        private static void ShowMenuText()
        {
            Console.WriteLine("Enter 'serialize' to create an array and serialize it to 'employees.xml'.");
            Console.WriteLine("Enter 'deserialize' to deserialize an array from 'employees.xml'.");
            Console.WriteLine("Enter 'all' to see all available employees.");
            Console.WriteLine("Enter 'special' to see and serialize emplyees 25<=age<=35 ordered by EmployeeID");
            Console.WriteLine("Enter 'exit' to exit.");
        }
    }
}

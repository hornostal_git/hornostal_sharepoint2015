/**
 * @author ���������� ������� ���������
 */

using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// �������� ������� ������� �� ���� "�����, ���������, Generic ����, Generic ���������" b: ���������.
/// </summary>
namespace Task02_b_Collections
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] sizes = new int[] { 100, 1000, 10000, 100000 };
            int[] arrayOfNumbers;
            string[] arrayOfStrings;
            List<int> listOfNumbers = new List<int>();
            ArrayList arrayListOfNumbers = new ArrayList();
            List<string> listOfStrings = new List<string>();
            ArrayList arrayListOfStrings = new ArrayList(); ;

            foreach (int size in sizes)
            {
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine("\n\n ------TESTING FOR {0} ELEMENTS:------\n", size);
                Console.ResetColor();
               

                arrayOfNumbers = Randomizer.GetRandomIntArray(size);
                arrayOfStrings = Randomizer.GetRandomStringArray(size, 5);
                listOfNumbers = new List<int>();
                arrayListOfNumbers = new ArrayList();
                listOfStrings = new List<string>();
                arrayListOfStrings = new ArrayList();

                ///////////////////////////////////////////////////////////////////////////////////////////
                // System.Int32
                ///////////////////////////////////////////////////////////////////////////////////////////
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\n System.Int32:", size);
                Console.ResetColor();

                Console.WriteLine("The time of adding elements:");
                Console.WriteLine("List<T>:\t{0}",
                    Timer.GetTimeAdd(listOfNumbers, arrayOfNumbers));
                Console.WriteLine("ArrayList:\t{0}",
                    Timer.GetTimeAdd(arrayListOfNumbers, arrayOfNumbers));

                Console.WriteLine("The time of sorting elements:");
                Console.WriteLine("List<T>:\t{0}",
                    Timer.GetTimeListSort(listOfNumbers));
                Console.WriteLine("ArrayList:\t{0}",
                    Timer.GetTimeArrayListSort(arrayListOfNumbers));

                Console.WriteLine("The time of popping elements:");
                Console.WriteLine("List<T>:\t{0}",
                    Timer.GetTimePop<int>(listOfNumbers));
                Console.WriteLine("ArrayList:\t{0}",
                    Timer.GetTimePop<int>(arrayListOfNumbers));


                ///////////////////////////////////////////////////////////////////////////////////////////
                // System.String
                ///////////////////////////////////////////////////////////////////////////////////////////
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\n System.String:", size);
                Console.ResetColor();


                Console.WriteLine("The time of adding elements:");
                Console.WriteLine("List<T>:\t{0}",
                   Timer.GetTimeAdd(listOfStrings, arrayOfStrings));
                Console.WriteLine("ArrayList:\t{0}",
                    Timer.GetTimeAdd(arrayListOfStrings, arrayOfStrings));

                Console.WriteLine("The time of sorting elements:");
                Console.WriteLine("List<T>:\t{0}",
                    Timer.GetTimeListSort(listOfStrings));
                Console.WriteLine("ArrayList:\t{0}",
                    Timer.GetTimeArrayListSort(arrayListOfStrings));

                Console.WriteLine("The time of popping elements:");
                Console.WriteLine("List<T>:\t{0}",
                    Timer.GetTimePop<string>(listOfStrings));
                Console.WriteLine("ArrayList:\t{0}",
                    Timer.GetTimePop<string>(arrayListOfStrings));
            }
            Console.ReadKey();
        }
    }
}
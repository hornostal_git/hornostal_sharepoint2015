﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Task02_b_Collections
{
    /// <summary>
    /// Хранит методы подсчёта времени проведения стандарнтых операций над коллекциями. 
    /// </summary>
    class Timer
    {
        /// <summary>
        /// Возвращает время добавления элементов в список из массива.
        /// </summary>
        /// <typeparam name="T"> Тип эллементов списка и массива</typeparam>
        /// <param name="list"> Список, в который добавляются элементы </param>
        /// <param name="array"> Массив, из которого копируются элементы </param>
        /// <returns> Время операции </returns>
        public static TimeSpan GetTimeAdd<T>(IList list, T[] array)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            for (int i = 0; i < array.Length; i++)
                list.Add(array[i]);
            timer.Stop();
            return timer.Elapsed;
        }
        
        /// <summary>
        /// Возвращает время получения элементов списка.
        /// </summary>
        /// <typeparam name="T"> Тип элементов списка </typeparam>
        /// <param name="list"> Список, из которого получают элементы </param>
        /// <returns> Время операции </returns>
        public static TimeSpan GetTimePop<T>(IList list)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            T variable;
            foreach (T currentValue in list)
                variable = currentValue;
            timer.Stop();
            return timer.Elapsed;
        }
        
        /// <summary>
        /// Возвращает время сортировки списка.
        /// </summary>
        /// <typeparam name="T"> Тип элементов списка </typeparam>
        /// <param name="list"> Сортируемый список </param>
        /// <returns> Время операции </returns>
        public static TimeSpan GetTimeListSort<T>(List<T> list)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            list.Sort();
            timer.Stop();
            return timer.Elapsed;
        }
        
        /// <summary>
        /// Возвращает время сортировки ArrayList.
        /// </summary>
        /// <param name="list"> Сортируемый ArrayList </param>
        /// <returns> Время операции </returns>
        public static TimeSpan GetTimeArrayListSort(ArrayList list)
        {
            Stopwatch timer = new Stopwatch();
            timer.Start();
            list.Sort();
            timer.Stop();
            return timer.Elapsed;
        }
     }
}
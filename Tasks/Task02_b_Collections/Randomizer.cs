﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;
using System.Text;

namespace Task02_b_Collections
{
    /// <summary>
    /// Хранит методы создания массивов с набором случайных элементов.
    /// </summary>
    static class Randomizer
    {
        private static Random random = new Random();

        /// <summary>
        /// Возвращает массив случайных целых чисел указанной длинны.
        /// </summary>
        /// <param name="length"> Необходимая длинна массива </param>
        /// <returns> Массив случайных целых чисел </returns>
        public static int[] GetRandomIntArray(int length)
        {
            int[] array = new int[length];
            for (int i = 0; i < length; i++)
                array[i] = random.Next();
            return array;
        }

        /// <summary>
        /// Возвращает массив случайных строк указнной длинны.
        /// </summary>
        /// <param name="length"> Необходимая длинна массива</param>
        /// <param name="lineSize"> Необходимая длинна строки </param>
        /// <returns> Массив случайных строк </returns>
        public static string[] GetRandomStringArray(int length, int lineSize)
        {
            string[] array = new string[length];
            for (int i = 0; i < length; i++)
                array[i] = GetRandomString(lineSize);
            return array;
        }

        /// <summary>
        /// Возвращает случайную строку [a-z] указаннной длинны.
        /// </summary>
        /// <param name="size"> Необходимый размер строки </param>
        /// <returns> Случайная строка </returns>
        private static string GetRandomString(int size)
        {
            StringBuilder builder = new StringBuilder();

            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(random.Next(97, 122));
                builder.Append(ch);
            }
            return builder.ToString();
        }
    }
}
﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;
using System.Collections.Generic;

namespace Task03_OOP
{
    /// <summary>
    /// Содержит в себе различные методы обработки коллекции живых существ
    /// </summary>
    static class Utility
    {
        public static void ShowAvailableCreatures(List<LivingCreatures> creatures)
        {
            Console.WriteLine("---------Available living creatures:---------");
            foreach (LivingCreatures livingCreature in creatures)
            {
                Console.WriteLine(livingCreature.ID);
            }
        }

        /// <summary>
        /// Считает количество ног у живых существ.
        /// </summary>
        /// <param name="creatures"> Колекция живыих существ </param>
        /// <returns> Количество ног </returns>
        public static int CalculateLegs(List<LivingCreatures> creatures)
        {
            int legs = 0;
            foreach (LivingCreatures livingBeing in creatures)
            {
                if (livingBeing is Animal)
                {
                    legs += ((Animal)livingBeing).Legs;
                }
            }
            return legs;
        }

        /// <summary>
        /// Ищет и выводит на экран всех живых существ, умеющих дышать.
        /// </summary>
        /// <param name="creatures"> Коллекция живых существ </param>
        public static void FindAllUnderwaterCreatures(List<LivingCreatures> creatures)
        {
            foreach (LivingCreatures livingBeing in creatures)
                if (livingBeing is ICanBreatheUnderwater)
                {
                    Console.WriteLine("\n   {0} can breathe underwater and it says: ", livingBeing.ID);
                    ((ICanBreatheUnderwater)livingBeing).BreatheUnderwater();
                }
        }
    }
}

﻿/**
 * @author Горносталь Алексей Андреевич
 */

namespace Task03_OOP
{
    /// <summary>
    /// Обеспечивает наличие умения "Дышать под водой" 
    /// </summary>
    interface ICanBreatheUnderwater
    {
        void BreatheUnderwater();
    }
}

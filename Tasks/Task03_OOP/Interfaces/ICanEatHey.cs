﻿/**
 * @author Горносталь Алексей Андреевич
 */

namespace Task03_OOP
{
    /// <summary>
    /// Обеспечивает наличие умения "Есть сено"
    /// </summary>
    interface ICanEatHey
    {
        void EatHay();
    }
}

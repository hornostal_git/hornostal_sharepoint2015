﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;

namespace Task03_OOP
{
    /// <summary>
    /// Реализует Сущность "Лошадь"
    /// </summary>
    class Horse : Animal, ICanEatHey
    {
        public void EatHay()
        {
            Console.WriteLine("I can eat hay");
        }
    }
}

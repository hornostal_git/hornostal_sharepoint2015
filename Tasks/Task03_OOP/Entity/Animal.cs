﻿/**
 * @author Горносталь Алексей Андреевич
 */

namespace Task03_OOP
{
    /// <summary>
    /// Реализует сущность "Животное"
    /// </summary>
    class Animal : LivingCreatures
    {
        /// <summary>
        /// По умолчанию у животного 4 ноги
        /// </summary>
        private int _legs = 4;

        /// <summary>
        /// Обеспечивает возможность извне получить количество ног.
        /// </summary>
        public int Legs {
            get { return _legs; }
        }
    }
}

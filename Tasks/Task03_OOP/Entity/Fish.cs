﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;

namespace Task03_OOP
{
    /// <summary>
    /// Реализует сущность "Рыба"
    /// </summary>
    class Fish : LivingCreatures, ICanBreatheUnderwater
    {
        public void BreatheUnderwater()
        {
            Console.WriteLine("I can breathe underwater!");
        }
    }
}

﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;

namespace Task03_OOP
{
    /// <summary>
    /// Реализует сущность "Карась"
    /// </summary>
    class Сrucian : Fish, ICanEatHey
    {
        public void EatHay()
        {
            Console.WriteLine("I can`t eat hey in the real life!");
        }
    }
}

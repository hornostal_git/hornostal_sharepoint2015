﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;

namespace Task03_OOP
{
    /// <summary>
    /// Реализует сущность "Собака"
    /// </summary>
    class Dog : Animal, ICanBreatheUnderwater
    {
        public void BreatheUnderwater()
        {
            Console.WriteLine("I can`t breathe underwater in the real life!");
        }
    }
}

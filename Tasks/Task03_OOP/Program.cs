﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;
using System.Collections.Generic;

/// <summary>
/// Содержит решение задания по теме "Основы ООП, применение ООП".
/// </summary>
namespace Task03_OOP
{
    class Program
    {
        static void Main(string[] args)
        {
            List<LivingCreatures> creatures = new List<LivingCreatures>
            {
                new Dog { ID = "Lucy" },
                new Roach { ID = "Jackie" },
                new Horse { ID = "Albert" },
                new Dog { ID = "Max" },
                new Roach { ID = "Sam" },
                new Horse { ID = "Speed" },
                new Dog { ID = "Toby" },
                new Сrucian { ID = "Shark" },
                new Horse { ID = "Rocket" }
            };

            Utility.ShowAvailableCreatures(creatures);

            Console.WriteLine("\n---------There are {0} legs!---------\n", Utility.CalculateLegs(creatures));

            Console.WriteLine("---------All creatures that can breathe uderwater:---------");
            Utility.FindAllUnderwaterCreatures(creatures);

            Console.ReadKey();
        }
    }
}

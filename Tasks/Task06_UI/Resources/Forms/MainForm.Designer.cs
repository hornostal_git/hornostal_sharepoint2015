﻿using System.Windows.Forms;

namespace Task06_UI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxFirstName = new System.Windows.Forms.TextBox();
            this.textBoxLastName = new System.Windows.Forms.TextBox();
            this.textBoxGroup = new System.Windows.Forms.TextBox();
            this.textBoxHomeNumber = new System.Windows.Forms.TextBox();
            this.textBoxMobileNumber = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.textBoxPhotoName = new System.Windows.Forms.TextBox();
            this.treeViewMain = new System.Windows.Forms.TreeView();
            this.labelFirstName = new System.Windows.Forms.Label();
            this.labelLastName = new System.Windows.Forms.Label();
            this.labelGroup = new System.Windows.Forms.Label();
            this.labelPhotoName = new System.Windows.Forms.Label();
            this.labelHomeNumber = new System.Windows.Forms.Label();
            this.labelMobileNumber = new System.Windows.Forms.Label();
            this.groupBoxMode = new System.Windows.Forms.GroupBox();
            this.radioButtonList = new System.Windows.Forms.RadioButton();
            this.radioButtonTree = new System.Windows.Forms.RadioButton();
            this.textBoxFirstNameSearch = new System.Windows.Forms.TextBox();
            this.labelSearch = new System.Windows.Forms.Label();
            this.listViewMain = new System.Windows.Forms.ListView();
            this.buttonBrowse = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBoxMode.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxFirstName
            // 
            this.textBoxFirstName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.textBoxFirstName.Location = new System.Drawing.Point(432, 74);
            this.textBoxFirstName.Name = "textBoxFirstName";
            this.textBoxFirstName.Size = new System.Drawing.Size(175, 20);
            this.textBoxFirstName.TabIndex = 2;
            // 
            // textBoxLastName
            // 
            this.textBoxLastName.Location = new System.Drawing.Point(432, 100);
            this.textBoxLastName.Name = "textBoxLastName";
            this.textBoxLastName.Size = new System.Drawing.Size(175, 20);
            this.textBoxLastName.TabIndex = 3;
            // 
            // textBoxGroup
            // 
            this.textBoxGroup.Location = new System.Drawing.Point(432, 126);
            this.textBoxGroup.Name = "textBoxGroup";
            this.textBoxGroup.Size = new System.Drawing.Size(175, 20);
            this.textBoxGroup.TabIndex = 4;
            // 
            // textBoxHomeNumber
            // 
            this.textBoxHomeNumber.Location = new System.Drawing.Point(418, 152);
            this.textBoxHomeNumber.Name = "textBoxHomeNumber";
            this.textBoxHomeNumber.Size = new System.Drawing.Size(88, 20);
            this.textBoxHomeNumber.TabIndex = 5;
            // 
            // textBoxMobileNumber
            // 
            this.textBoxMobileNumber.Location = new System.Drawing.Point(587, 152);
            this.textBoxMobileNumber.Name = "textBoxMobileNumber";
            this.textBoxMobileNumber.Size = new System.Drawing.Size(113, 20);
            this.textBoxMobileNumber.TabIndex = 6;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(341, 204);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(359, 240);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            // 
            // buttonSave
            // 
            this.buttonSave.Enabled = false;
            this.buttonSave.Location = new System.Drawing.Point(484, 450);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 8;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.button_Save);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(257, 450);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(75, 23);
            this.buttonDelete.TabIndex = 11;
            this.buttonDelete.Text = " Delete";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.button_Delete);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(28, 450);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.Size = new System.Drawing.Size(75, 23);
            this.buttonAdd.TabIndex = 12;
            this.buttonAdd.Text = "Add";
            this.buttonAdd.UseVisualStyleBackColor = true;
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // textBoxPhotoName
            // 
            this.textBoxPhotoName.Location = new System.Drawing.Point(468, 178);
            this.textBoxPhotoName.Name = "textBoxPhotoName";
            this.textBoxPhotoName.ReadOnly = true;
            this.textBoxPhotoName.Size = new System.Drawing.Size(139, 20);
            this.textBoxPhotoName.TabIndex = 7;
            // 
            // treeViewMain
            // 
            this.treeViewMain.Location = new System.Drawing.Point(28, 74);
            this.treeViewMain.Name = "treeViewMain";
            this.treeViewMain.Size = new System.Drawing.Size(304, 370);
            this.treeViewMain.TabIndex = 13;
            this.treeViewMain.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            // 
            // labelFirstName
            // 
            this.labelFirstName.AutoSize = true;
            this.labelFirstName.Location = new System.Drawing.Point(338, 77);
            this.labelFirstName.Name = "labelFirstName";
            this.labelFirstName.Size = new System.Drawing.Size(57, 13);
            this.labelFirstName.TabIndex = 14;
            this.labelFirstName.Text = "First Name";
            // 
            // labelLastName
            // 
            this.labelLastName.AutoSize = true;
            this.labelLastName.Location = new System.Drawing.Point(338, 100);
            this.labelLastName.Name = "labelLastName";
            this.labelLastName.Size = new System.Drawing.Size(58, 13);
            this.labelLastName.TabIndex = 15;
            this.labelLastName.Text = "Last Name";
            // 
            // labelGroup
            // 
            this.labelGroup.AutoSize = true;
            this.labelGroup.Location = new System.Drawing.Point(338, 126);
            this.labelGroup.Name = "labelGroup";
            this.labelGroup.Size = new System.Drawing.Size(36, 13);
            this.labelGroup.TabIndex = 16;
            this.labelGroup.Text = "Group";
            // 
            // labelPhotoName
            // 
            this.labelPhotoName.AutoSize = true;
            this.labelPhotoName.Location = new System.Drawing.Point(338, 181);
            this.labelPhotoName.Name = "labelPhotoName";
            this.labelPhotoName.Size = new System.Drawing.Size(88, 13);
            this.labelPhotoName.TabIndex = 17;
            this.labelPhotoName.Text = "Photo`s file name";
            // 
            // labelHomeNumber
            // 
            this.labelHomeNumber.AutoSize = true;
            this.labelHomeNumber.Location = new System.Drawing.Point(338, 155);
            this.labelHomeNumber.Name = "labelHomeNumber";
            this.labelHomeNumber.Size = new System.Drawing.Size(46, 13);
            this.labelHomeNumber.TabIndex = 18;
            this.labelHomeNumber.Text = "H tel. №";
            // 
            // labelMobileNumber
            // 
            this.labelMobileNumber.AutoSize = true;
            this.labelMobileNumber.Location = new System.Drawing.Point(509, 155);
            this.labelMobileNumber.Name = "labelMobileNumber";
            this.labelMobileNumber.Size = new System.Drawing.Size(47, 13);
            this.labelMobileNumber.TabIndex = 19;
            this.labelMobileNumber.Text = "M tel. №";
            // 
            // groupBoxMode
            // 
            this.groupBoxMode.Controls.Add(this.radioButtonList);
            this.groupBoxMode.Controls.Add(this.radioButtonTree);
            this.groupBoxMode.Location = new System.Drawing.Point(28, 15);
            this.groupBoxMode.Name = "groupBoxMode";
            this.groupBoxMode.Size = new System.Drawing.Size(107, 44);
            this.groupBoxMode.TabIndex = 20;
            this.groupBoxMode.TabStop = false;
            this.groupBoxMode.Text = "Select mode";
            // 
            // radioButtonList
            // 
            this.radioButtonList.AutoSize = true;
            this.radioButtonList.Location = new System.Drawing.Point(59, 19);
            this.radioButtonList.Name = "radioButtonList";
            this.radioButtonList.Size = new System.Drawing.Size(41, 17);
            this.radioButtonList.TabIndex = 21;
            this.radioButtonList.TabStop = true;
            this.radioButtonList.Text = "List";
            this.radioButtonList.UseVisualStyleBackColor = true;
            this.radioButtonList.CheckedChanged += new System.EventHandler(this.radioButtonList_CheckedChanged);
            // 
            // radioButtonTree
            // 
            this.radioButtonTree.AutoSize = true;
            this.radioButtonTree.Location = new System.Drawing.Point(6, 19);
            this.radioButtonTree.Name = "radioButtonTree";
            this.radioButtonTree.Size = new System.Drawing.Size(47, 17);
            this.radioButtonTree.TabIndex = 22;
            this.radioButtonTree.TabStop = true;
            this.radioButtonTree.Text = "Tree";
            this.radioButtonTree.UseVisualStyleBackColor = true;
            this.radioButtonTree.CheckedChanged += new System.EventHandler(this.radioButtonTree_CheckedChanged);
            // 
            // textBoxFirstNameSearch
            // 
            this.textBoxFirstNameSearch.Location = new System.Drawing.Point(208, 27);
            this.textBoxFirstNameSearch.Name = "textBoxFirstNameSearch";
            this.textBoxFirstNameSearch.Size = new System.Drawing.Size(100, 20);
            this.textBoxFirstNameSearch.TabIndex = 23;
            this.textBoxFirstNameSearch.TextChanged += new System.EventHandler(this.textBoxFirstNameSearch_TextChanged);
            // 
            // labelSearch
            // 
            this.labelSearch.AutoSize = true;
            this.labelSearch.Location = new System.Drawing.Point(154, 31);
            this.labelSearch.Name = "labelSearch";
            this.labelSearch.Size = new System.Drawing.Size(41, 13);
            this.labelSearch.TabIndex = 25;
            this.labelSearch.Text = "Search";
            // 
            // listViewMain
            // 
            this.listViewMain.Location = new System.Drawing.Point(28, 74);
            this.listViewMain.Name = "listViewMain";
            this.listViewMain.Size = new System.Drawing.Size(304, 370);
            this.listViewMain.TabIndex = 29;
            this.listViewMain.UseCompatibleStateImageBehavior = false;
            this.listViewMain.View = System.Windows.Forms.View.List;
            this.listViewMain.SelectedIndexChanged += new System.EventHandler(this.listView1_SelectedIndexChanged);
            // 
            // buttonBrowse
            // 
            this.buttonBrowse.Enabled = false;
            this.buttonBrowse.Location = new System.Drawing.Point(613, 176);
            this.buttonBrowse.Name = "buttonBrowse";
            this.buttonBrowse.Size = new System.Drawing.Size(79, 23);
            this.buttonBrowse.TabIndex = 8;
            this.buttonBrowse.Text = "Browse";
            this.buttonBrowse.UseVisualStyleBackColor = true;
            this.buttonBrowse.Click += new System.EventHandler(this.buttonBrowse_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(709, 488);
            this.Controls.Add(this.buttonBrowse);
            this.Controls.Add(this.listViewMain);
            this.Controls.Add(this.labelSearch);
            this.Controls.Add(this.textBoxFirstNameSearch);
            this.Controls.Add(this.groupBoxMode);
            this.Controls.Add(this.labelMobileNumber);
            this.Controls.Add(this.labelHomeNumber);
            this.Controls.Add(this.labelPhotoName);
            this.Controls.Add(this.labelGroup);
            this.Controls.Add(this.labelLastName);
            this.Controls.Add(this.labelFirstName);
            this.Controls.Add(this.treeViewMain);
            this.Controls.Add(this.textBoxPhotoName);
            this.Controls.Add(this.buttonSave);
            this.Controls.Add(this.buttonAdd);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBoxMobileNumber);
            this.Controls.Add(this.textBoxHomeNumber);
            this.Controls.Add(this.textBoxGroup);
            this.Controls.Add(this.textBoxLastName);
            this.Controls.Add(this.textBoxFirstName);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainForm";
            this.Text = "Contacts` manager";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBoxMode.ResumeLayout(false);
            this.groupBoxMode.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }        

        #endregion
        private TextBox textBoxFirstName;
        private TextBox textBoxLastName;
        private TextBox textBoxGroup;
        private TextBox textBoxHomeNumber;
        private TextBox textBoxMobileNumber;
        private TextBox textBoxPhotoName;
        private PictureBox pictureBox1;
        private Button buttonDelete;
        private Button buttonAdd;
        private Button buttonSave;
        private TreeView treeViewMain;
        private Label labelFirstName;
        private Label labelLastName;
        private Label labelGroup;
        private Label labelPhotoName;
        private Label labelHomeNumber;
        private Label labelMobileNumber;
        private GroupBox groupBoxMode;
        private RadioButton radioButtonList;
        private RadioButton radioButtonTree;
        private TextBox textBoxFirstNameSearch;
        private Label labelSearch;
        private ListView listViewMain;
        private Button buttonBrowse;
    }
}


﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Task06_Core;

/// <summary>
/// Содержит пользовательский интерфейс "Менеджера контактов" для решения задания по теме "Linq, лямбда выражения".
/// </summary>
namespace Task06_UI
{
    /// <summary>
    /// Часть, содержащая обработку событий главного окна.
    /// </summary>
    public partial class MainForm : Form
    {
        /// <summary>
        /// Изображение, загружающееся по-умолчанию при отсутвии другого или в случае ошибки.
        /// </summary>
        public static Image ImageDefault;

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            try
            {
                ImageDefault = Image.FromFile(Strings.DefaultImageName);
            } catch { }
            radioButtonTree.Checked = true;
            listViewMain.Visible = false;
            UpdateTreeView(ContactsUtility.GroupByGroup(Repository.Contacts));
            pictureBox1.Image = ImageDefault;            
        }

        #region Методы обработки событий от элементов окна.

        /// <summary>
        /// Обрабатывает нажатие кнопки Delete (удаляет выделенный контакта).
        /// </summary>
        private void button_Delete(object sender, EventArgs e)
        {            
            string selectedText;
            if (treeViewMain.Visible)
            {
                if (treeViewMain.SelectedNode == null) return;
                selectedText = treeViewMain.SelectedNode.Text;
                Repository.RemoveElement(selectedText);
                UpdateTreeView(ContactsUtility.GroupByGroup(Repository.Contacts));
            }
            else
            {
                if (listViewMain.SelectedItems.Count == 0) return;
                selectedText = listViewMain.SelectedItems[0].Text;
                Repository.RemoveElement(selectedText);
                UpdateListView(Repository.Contacts);
            }
            ResetTextBoxes();
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки Save (сохраняет внесённые изменения).
        /// </summary>
        private void button_Save(object sender, System.EventArgs e)
        {
            Contact newContact = ValidateFields();
            if (newContact != null)
            {
                Contact oldContact;
                if (treeViewMain.Visible)
                {
                    oldContact = ContactsUtility.FindContactByLine(Repository.Contacts, treeViewMain.SelectedNode.Text);
                    Repository.EditElement(oldContact, newContact);
                    UpdateTreeView(ContactsUtility.GroupByGroup(Repository.Contacts));
                }
                else
                {
                    oldContact = ContactsUtility.FindContactByLine(Repository.Contacts, listViewMain.SelectedItems[0].Text);
                    Repository.EditElement(oldContact, newContact);
                    listViewMain.SelectedItems[0].Text = newContact.ToString();
                }                
                ResetTextBoxes();
            }
        }

        /// <summary>
        /// Обрабатывает событие после выбора элемента TreeView (в правой части окна отображает инфомрацию для редактирования). 
        /// </summary>
        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string selectedText = treeViewMain.SelectedNode.Text;
            Contact contact = ContactsUtility.FindContactByLine(Repository.Contacts, selectedText);
            if (contact != null)
            {
                ShowContact(contact);
            }
        }

        /// <summary>
        /// Обрабатывает событие после выбора элемента ListView (в правой части окна отображает инфомрацию для редактирования). 
        /// </summary>
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewMain.SelectedIndices.Count <= 0)
            {
                return;
            }
            ShowContact(ContactsUtility.FindContactByLine(Repository.Contacts, listViewMain.SelectedItems[0].Text));
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки Add (открывает окно доабвления контакта).
        /// </summary>
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            int beginAmount = Repository.Contacts.Count();
            new AddForm().ShowDialog();
            if (beginAmount != Repository.Contacts.Count())
            {
                if (treeViewMain.Visible)
                {
                    UpdateTreeView(ContactsUtility.GroupByGroup(Repository.Contacts));
                }
                else
                {
                    UpdateListView(Repository.Contacts);
                }
            }
        }

        /// <summary>
        /// Обрабатывает изменение в выборе радиокнопки List (отображает или скрывает "список" контактов).
        /// </summary>
        private void radioButtonList_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonList.Checked)
            {
                listViewMain.Visible = true;
                UpdateListView(Repository.Contacts);
            }
            else
            {
                listViewMain.Visible = false;
            }
        }        

        /// <summary>
        /// Обрабатывает нажатие кнопки Reset (сбрасывает результаты поиска).
        /// </summary>
        private void buttonReset_Click(object sender, EventArgs e)
        {
            if (treeViewMain.Visible)
            {
                UpdateTreeView(ContactsUtility.GroupByGroup(Repository.Contacts));
            }
            else
            {
                UpdateListView(Repository.Contacts);
            }
            textBoxFirstNameSearch.Text = "";
        }

        /// <summary>
        /// Обрабатывает изменение в выборе радиокнопки Tree (отображает или скрывает "дерево" контактов).
        /// </summary>
        private void radioButtonTree_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonTree.Checked)
            {
                treeViewMain.Visible = true;
                UpdateTreeView(ContactsUtility.GroupByGroup(Repository.Contacts));
            }
            else
            {
                treeViewMain.Visible = false;
            }
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки Check (проверяет корректность имени файла с изображением).
        /// </summary>
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (System.IO.Directory.Exists(Strings.PathToResources + Strings.PhotosFolderName))
                openFileDialog1.InitialDirectory = System.IO.Path.GetFullPath(Strings.PathToResources + Strings.PhotosFolderName);
            else
                openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Images (*.BMP;*.JPG;*.GIF;*.TIFF)|*.BMP;*.JPG;*.GIF;*.TIFF";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxPhotoName.Text = openFileDialog1.FileName;
            }
            try
            {
                labelPhotoName.ForeColor = Color.Black;
                labelPhotoName.Text = "Photo`s file name";
                Image image = Image.FromFile(textBoxPhotoName.Text);
                pictureBox1.Image = image;
            }
            catch
            {
                labelPhotoName.ForeColor = Color.Red;
                labelPhotoName.Text = "Invalid file`s name";
                pictureBox1.Image = MainForm.ImageDefault;
            }
        }

        /// <summary>
        /// Обрабатывает изменение текста в textBoxFirstNameSearch (проводится поиск по списку контактов).
        /// </summary>
        private void textBoxFirstNameSearch_TextChanged(object sender, EventArgs e)
        {
            string firstName = textBoxFirstNameSearch.Text;
            IEnumerable<Contact> temp = Repository.Contacts;
            if (!string.IsNullOrWhiteSpace(firstName))
            {
                temp = temp.Where(contact => contact.ToString().IndexOf(firstName, StringComparison.OrdinalIgnoreCase) >= 0);
            }
            if (treeViewMain.Visible)
            {
                UpdateTreeView(ContactsUtility.GroupByGroup(temp));
            }
            else
            {
                UpdateListView(temp);
            }
        }

        #endregion

        #region Методы обновленния содержимого элементов окна.

        /// <summary>
        /// Обновляет содержимое TreeView.
        /// </summary>
        /// <param name="groups"> Группы контактов по группам. </param>
        private void UpdateTreeView(IEnumerable<IGrouping<string, Contact>> groups)
        {
            treeViewMain.Nodes.Clear();
            int index = 0;
            foreach (var group in groups)
            {
                treeViewMain.Nodes.Add(group.Key);
                foreach (var g in group)
                {
                    treeViewMain.Nodes[index].Nodes.Add(g.ToString());
                }
                index++;
            }
            treeViewMain.ExpandAll();
        }

        /// <summary>
        /// Обновляет содержимое ListView.
        /// </summary>
        /// <param name="contacts"> Контакты для отображения. </param>
        private void UpdateListView(IEnumerable<Contact> contacts)
        {
            if (listViewMain.Visible)
            {
                listViewMain.Clear();
                foreach (var contact in contacts)
                {
                    listViewMain.Items.Add(contact.ToString());
                }
            }
        }

        /// <summary>
        /// Отображает информацию контакта для редактиврования в правой части окна.
        /// </summary>
        /// <param name="contact"> Контакт, инфомарцию о ктором необходимо отобразить. </param>
        private void ShowContact(Contact contact)
        {
            buttonSave.Enabled = true;
            buttonBrowse.Enabled = true;
            try
            {
                textBoxFirstName.Text = contact.FirstName;
                textBoxLastName.Text = contact.LastName;
                textBoxGroup.Text = contact.Group;
                textBoxHomeNumber.Text = contact.HomeTelephoneNumber;
                textBoxMobileNumber.Text = contact.MobileTelephoneNumber;
                textBoxPhotoName.Text = contact.PhotoName;
            }
            catch { }
            try
            {
                Image image = Image.FromFile(contact.PhotoName);
                pictureBox1.Image = image;
            }
            catch
            {
                pictureBox1.Image = MainForm.ImageDefault;
            }
        }

        /// <summary>
        /// Очищает текстовые поля редактирования контакта.
        /// </summary>
        private void ResetTextBoxes()
        {
            textBoxFirstName.Text = "";
            textBoxLastName.Text = "";
            textBoxGroup.Text = "";
            textBoxHomeNumber.Text = "";
            textBoxMobileNumber.Text = "";
            textBoxPhotoName.Text = "";
            textBoxPhotoName.Text = "";
            pictureBox1.Image = MainForm.ImageDefault;
            buttonSave.Enabled = false;
            buttonBrowse.Enabled = false;
        }

        /// <summary>
        /// Обеспечивает валидацию вводмых данных.
        /// </summary>
        /// <returns> Новый контакт, если все поля прошли проверку. </returns>
        /// <returns> null, если какое-то поле проверку не прошло. </returns>
        private Contact ValidateFields()
        {
            bool isValid = true;
            Contact contact = new Contact();
            if (StringsValidator.IsValidName(textBoxFirstName.Text))
            {
                labelFirstName.ForeColor = Color.Black;
                contact.FirstName = textBoxFirstName.Text;
                labelFirstName.Text = "First Name";
            }
            else
            {
                labelFirstName.ForeColor = Color.Red;
                labelFirstName.Text = "Invalid First Name";
                isValid = false;
            }

            if (StringsValidator.IsValidName(textBoxLastName.Text))
            {
                labelLastName.ForeColor = Color.Black;
                contact.LastName = textBoxLastName.Text;
                labelLastName.Text = "Last Name";
            }
            else
            {
                    labelLastName.ForeColor = Color.Red;
                    labelLastName.Text = "Invalid Last Name";
                isValid = false;
            }

            if (StringsValidator.IsValidGroup(textBoxGroup.Text))
            {
                labelGroup.ForeColor = Color.Black;
                contact.Group = textBoxGroup.Text;
                labelGroup.Text = "Group";
            }
            else
            {
                labelGroup.ForeColor = Color.Red;
                labelGroup.Text = "Invalid Group";
                isValid = false;
            }

            if (StringsValidator.IsValidNumber(textBoxHomeNumber.Text))
            {
                labelHomeNumber.ForeColor = Color.Black;
                contact.HomeTelephoneNumber = textBoxHomeNumber.Text;
                labelHomeNumber.Text = "H tel. №";
            }
            else
            {
                labelHomeNumber.ForeColor = Color.Red;
                labelHomeNumber.Text = "Invalid number";
                isValid = false;
            }

            if (StringsValidator.IsValidNumber(textBoxMobileNumber.Text))
            {
                labelMobileNumber.ForeColor = Color.Black;
                contact.MobileTelephoneNumber = textBoxMobileNumber.Text;
                labelMobileNumber.Text = "M tel. №";
            }
            else
            {
                labelMobileNumber.ForeColor = Color.Red;
                labelMobileNumber.Text = "Invalid number";
                isValid = false;
            }

            if (StringsValidator.IsValidImageFileName(textBoxPhotoName.Text))
            {
                labelPhotoName.ForeColor = Color.Black;
                contact.PhotoName = textBoxPhotoName.Text;
                labelPhotoName.Text = "Photo`s file name";
            }
            else
            {
                labelPhotoName.ForeColor = Color.Red;
                labelPhotoName.Text = "Invalid file`s name";
                isValid = false;
            }

            if (isValid)
            {
                return contact;
            }
            else
            {
                return null;
            }
        }

        #endregion        
    }
}
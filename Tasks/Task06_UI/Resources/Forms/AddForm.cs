﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System;
using System.Drawing;
using System.Windows.Forms;
using Task06_Core;

namespace Task06_UI
{
    /// <summary>
    /// Часть, содержащая обработку событий окна добавления контактов.
    /// </summary>
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
            pictureBox1.Image = MainForm.ImageDefault;
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки Add (добавляет контакт в список).
        /// </summary>
        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Contact newContact = ValidateFields();
            if (newContact == null) return;
            Repository.AddElement(newContact);
            this.Close();
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки Check (проверяет корректность имени файла с изображением).
        /// </summary>
        private void buttonBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            if (System.IO.Directory.Exists(Strings.PathToResources + Strings.PhotosFolderName))
                openFileDialog1.InitialDirectory = System.IO.Path.GetFullPath(Strings.PathToResources + Strings.PhotosFolderName);
            else
                openFileDialog1.InitialDirectory = "c:\\";
            openFileDialog1.Filter = "Images (*.BMP;*.JPG;*.GIF;*.TIFF)|*.BMP;*.JPG;*.GIF;*.TIFF";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                textBoxPhotoName.Text = openFileDialog1.FileName;
            }
            try
            {
                labelPhotoName.ForeColor = Color.Black;
                labelPhotoName.Text = "Photo`s file name";
                Image image = Image.FromFile(textBoxPhotoName.Text);
                pictureBox1.Image = image;
            }
            catch
            {
                labelPhotoName.ForeColor = Color.Red;
                labelPhotoName.Text = "Invalid file`s name";
                pictureBox1.Image = MainForm.ImageDefault;
            }
        }

        /// <summary>
        /// Обеспечивает валидацию вводмых данных.
        /// </summary>
        /// <returns> Новый контакт, если все поля прошли проверку. </returns>
        /// <returns> null, если какое-то поле проверку не прошло. </returns>
        private Contact ValidateFields()
        {
            bool isValid = true;
            Contact contact = new Contact();
            if (StringsValidator.IsValidName(textBoxFirstName.Text))
            {
                labelFirstName.ForeColor = Color.Black;
                contact.FirstName = textBoxFirstName.Text;
                labelFirstName.Text = "First Name";
            }
            else
            {
                labelFirstName.ForeColor = Color.Red;
                labelFirstName.Text = "Invalid First Name";
                isValid = false;
            }

            if (StringsValidator.IsValidName(textBoxLastName.Text))
            {
                labelLastName.ForeColor = Color.Black;
                contact.LastName = textBoxLastName.Text;
                labelLastName.Text = "Last Name";
            }
            else
            {
                labelLastName.ForeColor = Color.Red;
                labelLastName.Text = "Invalid Last Name";
                isValid = false;
            }

            if (StringsValidator.IsValidGroup(textBoxGroup.Text))
            {
                labelGroup.ForeColor = Color.Black;
                contact.Group = textBoxGroup.Text;
                labelGroup.Text = "Group";
            }
            else
            {
                labelGroup.ForeColor = Color.Red;
                labelGroup.Text = "Invalid Group";
                isValid = false;
            }

            if (StringsValidator.IsValidNumber(textBoxHomeNumber.Text))
            {
                labelHomeNumber.ForeColor = Color.Black;
                contact.HomeTelephoneNumber = textBoxHomeNumber.Text;
                labelHomeNumber.Text = "H tel. №";
            }
            else
            {
                labelHomeNumber.ForeColor = Color.Red;
                labelHomeNumber.Text = "Invalid number";
                isValid = false;
            }

            if (StringsValidator.IsValidNumber(textBoxMobileNumber.Text))
            {
                labelMobileNumber.ForeColor = Color.Black;
                contact.MobileTelephoneNumber = textBoxMobileNumber.Text;
                labelMobileNumber.Text = "M tel. №";
            }
            else
            {
                labelMobileNumber.ForeColor = Color.Red;
                labelMobileNumber.Text = "Invalid number";
                isValid = false;
            }

            if (StringsValidator.IsValidImageFileName(textBoxPhotoName.Text))
            {
                labelPhotoName.ForeColor = Color.Black;
                contact.PhotoName = textBoxPhotoName.Text;
                labelPhotoName.Text = "Photo`s file name";
            }
            else
            {
                labelPhotoName.ForeColor = Color.Red;
                labelPhotoName.Text = "Invalid file`s name";
                isValid = false;
            }

            if (isValid)
            {
                return contact;
            }
            else
            {
                return null;
            }
        }
        }
}

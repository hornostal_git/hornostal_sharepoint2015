﻿namespace Task07_final_UI
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.treeViewMain = new System.Windows.Forms.TreeView();
            this.textBoxDirectoryPath = new System.Windows.Forms.TextBox();
            this.buttonGo = new System.Windows.Forms.Button();
            this.richTextBoxConsole = new System.Windows.Forms.RichTextBox();
            this.labelDirectoryPath = new System.Windows.Forms.Label();
            this.labelFileResultPath = new System.Windows.Forms.Label();
            this.textBoxFileResultPath = new System.Windows.Forms.TextBox();
            this.buttonFileResultPath = new System.Windows.Forms.Button();
            this.buttonDirectoryPath = new System.Windows.Forms.Button();
            this.labelConsole = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.listViewErrors = new System.Windows.Forms.ListView();
            this.SuspendLayout();
            // 
            // treeViewMain
            // 
            this.treeViewMain.Location = new System.Drawing.Point(12, 12);
            this.treeViewMain.Name = "treeViewMain";
            this.treeViewMain.Size = new System.Drawing.Size(415, 285);
            this.treeViewMain.TabIndex = 2;
            // 
            // textBoxDirectoryPath
            // 
            this.textBoxDirectoryPath.Location = new System.Drawing.Point(120, 303);
            this.textBoxDirectoryPath.Name = "textBoxDirectoryPath";
            this.textBoxDirectoryPath.ReadOnly = true;
            this.textBoxDirectoryPath.Size = new System.Drawing.Size(307, 20);
            this.textBoxDirectoryPath.TabIndex = 3;
            // 
            // buttonGo
            // 
            this.buttonGo.Location = new System.Drawing.Point(302, 520);
            this.buttonGo.Name = "buttonGo";
            this.buttonGo.Size = new System.Drawing.Size(97, 23);
            this.buttonGo.TabIndex = 4;
            this.buttonGo.Text = "Go";
            this.buttonGo.UseVisualStyleBackColor = true;
            this.buttonGo.Click += new System.EventHandler(this.buttonGo_Click);
            // 
            // richTextBoxConsole
            // 
            this.richTextBoxConsole.Location = new System.Drawing.Point(12, 431);
            this.richTextBoxConsole.Name = "richTextBoxConsole";
            this.richTextBoxConsole.Size = new System.Drawing.Size(415, 66);
            this.richTextBoxConsole.TabIndex = 6;
            this.richTextBoxConsole.Text = "";
            // 
            // labelDirectoryPath
            // 
            this.labelDirectoryPath.AutoSize = true;
            this.labelDirectoryPath.Location = new System.Drawing.Point(12, 307);
            this.labelDirectoryPath.Name = "labelDirectoryPath";
            this.labelDirectoryPath.Size = new System.Drawing.Size(102, 13);
            this.labelDirectoryPath.TabIndex = 8;
            this.labelDirectoryPath.Text = "Путь к директории";
            // 
            // labelFileResultPath
            // 
            this.labelFileResultPath.AutoSize = true;
            this.labelFileResultPath.Location = new System.Drawing.Point(12, 359);
            this.labelFileResultPath.Name = "labelFileResultPath";
            this.labelFileResultPath.Size = new System.Drawing.Size(151, 13);
            this.labelFileResultPath.TabIndex = 10;
            this.labelFileResultPath.Text = "Путь к файлу с результатом";
            // 
            // textBoxFileResultPath
            // 
            this.textBoxFileResultPath.Location = new System.Drawing.Point(169, 355);
            this.textBoxFileResultPath.Name = "textBoxFileResultPath";
            this.textBoxFileResultPath.ReadOnly = true;
            this.textBoxFileResultPath.Size = new System.Drawing.Size(258, 20);
            this.textBoxFileResultPath.TabIndex = 9;
            // 
            // buttonFileResultPath
            // 
            this.buttonFileResultPath.Location = new System.Drawing.Point(302, 382);
            this.buttonFileResultPath.Name = "buttonFileResultPath";
            this.buttonFileResultPath.Size = new System.Drawing.Size(124, 23);
            this.buttonFileResultPath.TabIndex = 11;
            this.buttonFileResultPath.Text = "Выбрать файл";
            this.buttonFileResultPath.UseVisualStyleBackColor = true;
            this.buttonFileResultPath.Click += new System.EventHandler(this.buttonBrowseFileResult_Click);
            // 
            // buttonDirectoryPath
            // 
            this.buttonDirectoryPath.Location = new System.Drawing.Point(302, 329);
            this.buttonDirectoryPath.Name = "buttonDirectoryPath";
            this.buttonDirectoryPath.Size = new System.Drawing.Size(124, 23);
            this.buttonDirectoryPath.TabIndex = 12;
            this.buttonDirectoryPath.Text = "Выбрать директорию";
            this.buttonDirectoryPath.UseVisualStyleBackColor = true;
            this.buttonDirectoryPath.Click += new System.EventHandler(this.buttonBrowseFolder_Click);
            // 
            // labelConsole
            // 
            this.labelConsole.AutoSize = true;
            this.labelConsole.Location = new System.Drawing.Point(12, 415);
            this.labelConsole.Name = "labelConsole";
            this.labelConsole.Size = new System.Drawing.Size(50, 13);
            this.labelConsole.TabIndex = 13;
            this.labelConsole.Text = "Консоль";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 504);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Количество ошибок";
            // 
            // listViewErrors
            // 
            this.listViewErrors.Location = new System.Drawing.Point(12, 520);
            this.listViewErrors.Name = "listViewErrors";
            this.listViewErrors.Size = new System.Drawing.Size(282, 53);
            this.listViewErrors.TabIndex = 15;
            this.listViewErrors.UseCompatibleStateImageBehavior = false;
            this.listViewErrors.View = System.Windows.Forms.View.List;
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(439, 591);
            this.Controls.Add(this.listViewErrors);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelConsole);
            this.Controls.Add(this.buttonDirectoryPath);
            this.Controls.Add(this.buttonFileResultPath);
            this.Controls.Add(this.labelFileResultPath);
            this.Controls.Add(this.textBoxFileResultPath);
            this.Controls.Add(this.labelDirectoryPath);
            this.Controls.Add(this.richTextBoxConsole);
            this.Controls.Add(this.buttonGo);
            this.Controls.Add(this.textBoxDirectoryPath);
            this.Controls.Add(this.treeViewMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormMain";
            this.RightToLeftLayout = true;
            this.Text = "Файловый менеджер";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormMain_FormClosing);
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TreeView treeViewMain;
        private System.Windows.Forms.TextBox textBoxDirectoryPath;
        private System.Windows.Forms.Button buttonGo;
        private System.Windows.Forms.RichTextBox richTextBoxConsole;
        private System.Windows.Forms.Label labelDirectoryPath;
        private System.Windows.Forms.Label labelFileResultPath;
        private System.Windows.Forms.TextBox textBoxFileResultPath;
        private System.Windows.Forms.Button buttonFileResultPath;
        private System.Windows.Forms.Button buttonDirectoryPath;
        private System.Windows.Forms.Label labelConsole;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView listViewErrors;
    }
}


﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Linq;

/// <summary>
/// Содержит решение задания по теме "Advanced Topics, Итоговое задание по C#".
/// </summary>
namespace Task07_final_UI
{
    /// <summary>
    /// Часть, содержащая основные методы и обработчики событий главного окна.
    /// </summary>
    public partial class FormMain : Form
    {
        #region Поля класса

        // Объект для блокировки участки кода.
        private readonly object _locker = new object();

        // Поток сбора данных.
        private Thread threadCollection;

        // Поток формирования TreeView.
        private Thread threadTreeView;

        // Поток формирвоания xml-файла.
        private Thread threadXmlTree;

        // Полный путь к текущей дирекотрии.
        private string _fullPathToDirectory = "";

        // Полный путь к файлу, в котором необходимо сохранить результат.
        private string _fullPathToResultFile = "";

        // Индекс начала обрабатываемой директории в строке полного пути к ней.
        int beginIndex;

        // Текущая директория для обработки
        private static DirectoryInfo curentDirectory;

        // Счётчик директорий.
        private int foldersCounter;

        // Потоки должны работать?
        private bool _threadShouldWork = true;

        // Очередь полных путей для потоков.
        private List<string> _mainQueue = new List<string>();

        // Счётчик для чтения путей для потока отображения иерархии папок и файлов на экране.
        private int _firstThreadReadingCounter;

        // Счётчик для чтения путей для потока записи иерархии папок и файлов в xml-структуру в файле.
        private int _secondThreadReadingCounter;

        // Коллекция коренных элементов TreeView главного окна.
        private TreeNodeCollection collection;

        // Первая часть пути к директории
        private string _firstPartOfDirectoryPath;

        #endregion




        #region Делегат и простые методы   

        /// <summary>
        /// Делегат для операции "добавление звена в элемент пользовательского интерфейса TreeView".
        /// </summary>
        /// <param name="treeNodes"> Имеющиеся звенья на уровне. </param>
        /// <param name="line"> Строка, содержащая инфомрацию для добавления. </param>
        /// <returns> Добавленный элемент. </returns>
        private delegate TreeNode addNodeToTreeView(TreeNodeCollection treeNodes, string line);

        /// <summary>
        /// Позволяет добавить текст в RichTextBox главного окна.
        /// </summary>
        /// <param name="text"> Текст, который необходимо доабвить. </param>
        private void appendTextToRichTextBox(string text)
        {
            richTextBoxConsole.AppendText(text);
        }

        /// <summary>
        /// Позволяет добавить текст в RichTextBox главного окна.
        /// </summary>
        /// <param name="text"> Текст, который необходимо доабвить. </param>
        private void appendTextToRichTextBox(string text, System.Drawing.Color color)
        {
            richTextBoxConsole.SelectionStart = richTextBoxConsole.TextLength;
            richTextBoxConsole.SelectionLength = 0;
            richTextBoxConsole.SelectionColor = color;
            richTextBoxConsole.AppendText(text + Environment.NewLine);
            richTextBoxConsole.SelectionColor = richTextBoxConsole.ForeColor;
        }

        /// <summary>
        /// Позволяет добавить звено в TreeView.
        /// </summary>
        /// <param name="treeNodes"> Имеющиеся звенья на уровне. </param>
        /// <param name="line"> Строка, содержащая инфомрацию для добавления. </param>
        /// <returns> Добавленный элемент. </returns>
        private TreeNode addNode(TreeNodeCollection treeNodes, string line)
        {
            return treeNodes.Add(line);
        }

        #endregion




        #region Обработчики событий окна

        // Конструктор окна
        public FormMain()
        {
            InitializeComponent();
        }

        // Обрабатывает событие "загрузка окна" и содаёт два дочерних потока.
        private void FormMain_Load(object sender, EventArgs e)
        {
            HandledException += OnExceptionCatched;
            threadTreeView = new Thread(StarBuildingTreeView);
            threadXmlTree = new Thread(StartSavingXmlTree);
            threadTreeView.IsBackground = true;
            threadXmlTree.IsBackground = true;
            threadTreeView.Start();
            threadXmlTree.Start();
        }

        // Обрабатывает нажатие кнопки Go (запуск поиска папок и файлов и их отображения).
        private void buttonGo_Click(object sender, EventArgs e)
        {
            treeViewMain.Nodes.Clear();
            collection = treeViewMain.Nodes;
            _fullPathToDirectory = textBoxDirectoryPath.Text;
            _fullPathToResultFile = textBoxFileResultPath.Text;
            try { threadCollection.Abort(); }
            catch { }
            lock (_locker)
            {
                _mainQueue.Clear();
            }
            _firstThreadReadingCounter = 0;
            _secondThreadReadingCounter = 0;
            Thread.Sleep(100);
            try
            {
                curentDirectory = new DirectoryInfo(_fullPathToDirectory);
                labelDirectoryPath.ForeColor = System.Drawing.Color.Black;
                labelDirectoryPath.Text = "Путь к директории";
            }
            catch
            {
                labelDirectoryPath.ForeColor = System.Drawing.Color.Red;
                labelDirectoryPath.Text = "Неправильный путь";
                return;
            }
            try
            {
                XDocument doc = new XDocument();
                doc.Add(new XElement("TreeOfFolders"));
                doc.Save(_fullPathToResultFile);
                labelFileResultPath.ForeColor = System.Drawing.Color.Black;
                labelFileResultPath.Text = "Путь к файлу с результатом";
            }
            catch
            {
                labelFileResultPath.ForeColor = System.Drawing.Color.Red;
                labelFileResultPath.Text = "Неправильный путь";
                return;
            }          
            richTextBoxConsole.InvokeIfRequired(o =>
                {
                    appendTextToRichTextBox("Начинаю работу для следующей директории:" + Environment.NewLine + curentDirectory.FullName, System.Drawing.Color.Green);
                }
            );
            errors.Clear();
            listViewErrors.Items.Clear();
            beginIndex = _fullPathToDirectory.LastIndexOf(Path.DirectorySeparatorChar);
            threadCollection = new Thread(StartCollectingData);
            threadCollection.Start();

        }

        // Обеспечивает выбор файла для сохранения результата работы программы
        private void buttonBrowseFileResult_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            saveFileDialog.Filter = "xml files (*.xml)|*.xml";
            saveFileDialog.FilterIndex = 1;
            saveFileDialog.RestoreDirectory = true;

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                textBoxFileResultPath.Text = saveFileDialog.FileName;
            }
        }

        // Обеспечивает выбор директории для обработки
        private void buttonBrowseFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                textBoxDirectoryPath.Text = fbd.SelectedPath;
            }
        }

        // Обрабатывает событие закрытия окна.
        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            threadTreeView.Abort();
            threadXmlTree.Abort();
            try
            {
                threadCollection.Abort();
            }
            catch { }
            lock (_locker)
            {
                _mainQueue.Clear();
            }
            _threadShouldWork = false;
            Thread.Sleep(500);
        }

        #endregion




        #region Разработанные методы для работы в потоках

        /// <summary>
        /// Запускает сбор инфомарции и отображает соответвующие информационные сообщения.
        /// </summary>
        void StartCollectingData()
        {
            richTextBoxConsole.InvokeIfRequired(o =>
            {
                appendTextToRichTextBox("Начался сбор данных...", System.Drawing.Color.Red);
                buttonGo.Enabled = false;
            }
            );
            _firstPartOfDirectoryPath = curentDirectory.FullName.Substring(0, beginIndex);
            FindDirectoriesAndFilesRecursively(curentDirectory);
            richTextBoxConsole.InvokeIfRequired(o =>
            {
                buttonGo.Enabled = true;
                appendTextToRichTextBox("Сбор данных окончен...", System.Drawing.Color.Green);
            }
            );
        }

        /// <summary>
        /// Рекурсивно ищет директории и файлы и записывает пути в очереди.
        /// </summary>
        /// <param name="directory"> Директория, в которой нужно произвести поиск файлов и папок. </param>
        void FindDirectoriesAndFilesRecursively(DirectoryInfo directory)
        {
            IEnumerable<FileInfo> files = new List<FileInfo>();
            try
            {
                files = directory.EnumerateFiles();
            }
            catch (Exception exception)
            {
                InvokeExceptionHandler(exception, Thread.CurrentThread.Name);
            }
            foreach (var file in files)
            {
                lock (_locker)
                {
                    try
                    {
                        _mainQueue.Add(file.FullName.Substring(beginIndex, file.FullName.Length - beginIndex));
                    }
                    catch (Exception exception)
                    {
                        InvokeExceptionHandler(exception, Thread.CurrentThread.Name);
                    }
                }
            }
            IEnumerable<DirectoryInfo> dirs = new List<DirectoryInfo>();
            try
            {
                dirs = directory.EnumerateDirectories();
            }
            catch (Exception exception)
            {
                InvokeExceptionHandler(exception, Thread.CurrentThread.Name);
            }
            foreach (var dir in dirs)
            {
                lock (_locker)
                {
                    _mainQueue.Add(dir.FullName.Substring(beginIndex, dir.FullName.Length - beginIndex));
                }
                FindDirectoriesAndFilesRecursively(dir);
            }
        }

        /// <summary>
        /// Зупускает процесс заполнения TreeView файлами и директориями. 
        /// </summary>
        void StarBuildingTreeView()
        {
            while (_threadShouldWork)
            {
                if (_firstThreadReadingCounter < _mainQueue.Count)
                {
                    string currentDirName;
                    lock (_locker)
                    {
                        currentDirName = _mainQueue[_firstThreadReadingCounter++];
                    }
                    string[] dirs = currentDirName.Split(new char[] { Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
                    AddNodeToTree(dirs);
                    foldersCounter++;
                }
            }
        }

        /// <summary>
        /// Добавляет файл или дирректорию в TreeView.
        /// </summary>
        /// <param name="hierarchicalSequence"> Массив, состоящий из иерархической последовательности папок. </param>
        void AddNodeToTree(string[] hierarchicalSequence)
        {
            TreeNodeCollection tempCollection = collection;
            int index = 0;
            bool end = false;
            bool shouldContinue = false;

            while (tempCollection.Count != 0 && !end)
            {
                shouldContinue = false;
                foreach (TreeNode n in tempCollection)
                {
                    if (n.Text == hierarchicalSequence[index])
                    {
                        tempCollection = n.Nodes;
                        index++;
                        shouldContinue = true;
                    }
                }
                if (shouldContinue)
                    continue;
                else end = true;
            }

            for (int i = index; i < hierarchicalSequence.Length; i++)
            {
                tempCollection = ((TreeNode)treeViewMain.Invoke(new addNodeToTreeView(addNode), tempCollection, hierarchicalSequence[i])).Nodes;
            }
        }

        /// <summary>
        /// Запускает процесс сохранения иерархии папок и файлов в xml.
        /// </summary>
        void StartSavingXmlTree()
        {
            while (_threadShouldWork)
            {
                if (_secondThreadReadingCounter < _mainQueue.Count)
                {
                    string currentDirName;
                    lock (_locker)
                    {
                        currentDirName = _mainQueue[_secondThreadReadingCounter++];
                    }
                    collection = treeViewMain.Nodes;
                    AddNodeToXmlTree(currentDirName);
                }
            }
        }

        /// <summary>
        /// Добавляет папку или файл в xml-файл с иерархической структурой.
        /// </summary>
        /// <param name="currentDirFullName"> Полный путь к файлу или папке. </param>
        private void AddNodeToXmlTree(string currentDirFullName)
        {
            string[] hierarchicalSequence = currentDirFullName.Split(new char[] { Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);
            XDocument doc;
            XElement element;
            try
            {
                doc = XDocument.Load(_fullPathToResultFile);
                element = doc.Element("TreeOfFolders");
            }
            catch (Exception exception)
            {
                InvokeExceptionHandler(exception, Thread.CurrentThread.Name);
                return;
            }
            int index = 0;
            bool end = false;
            bool shouldContinue = false;

            while (element.Elements().Count() != 0 && !end)
            {
                shouldContinue = false;
                foreach (var n in element.Elements())
                {
                    if (n.Attribute("Name") != null && n.Attribute("Name").Value == hierarchicalSequence[index])
                    {
                        element = n;
                        index++;
                        shouldContinue = true;
                    }
                }
                if (shouldContinue)
                    continue;
                else end = true;
            }

            for (int i = index; i < hierarchicalSequence.Length - 1; i++)
            {
                element.Add(new XElement("dir", new XAttribute("Name", hierarchicalSequence[i])));
                element = element.Elements("dir").First(c => c.Attribute("Name").Value == hierarchicalSequence[i]);
            }

            FileInfo file = new FileInfo(_firstPartOfDirectoryPath + currentDirFullName);

            if ((file.Attributes & FileAttributes.Directory)
                 == FileAttributes.Directory)
            {
                element.Add(new XElement("dir",
                    new XAttribute("Name", file.Name)));
            }
            else
            {
                element.Add(new XElement("File",
                    new XAttribute("Name", file.Name),
                    new XElement("Length", file.Length)));
            }
            doc.Save(_fullPathToResultFile);
        }

        #endregion




        #region Система обработки исключений

        /// <summary>
        /// Содержит информацию о пойманном исключении.
        /// </summary>
        public class ExceptionCatchedEvent : EventArgs
        {
            public ExceptionCatchedEvent(string threadName, Exception exception)
            {
                ThreadName = threadName;
                CurrentException = exception;
            }

            /// <summary>
            /// Имя потока, в котором поймано исключение.
            /// </summary>
            public string ThreadName { get; set; }

            /// <summary>
            /// Текущее исключение в потоке.
            /// </summary>
            public Exception CurrentException { get; set; }
        }

        /// <summary>
        /// Очередь исключений.
        /// </summary>
        private static Queue<ExceptionCatchedEvent> eventsQueue = new Queue<ExceptionCatchedEvent>();

        /// <summary>
        /// Событие для обработки исключения и последующего вывода сообщения.
        /// </summary>
        public event EventHandler<ExceptionCatchedEvent> HandledException;

        /// <summary>
        /// Вызывается при поимке исключения.
        /// </summary>
        /// <param name="exception"> Выброшенное исключение. </param>
        /// <param name="nameOfThread"> Имя потока с исключением. </param>
        private void InvokeExceptionHandler(Exception exception, string nameOfThread)
        {
            lock (_locker)
            {
                eventsQueue.Enqueue(new ExceptionCatchedEvent(nameOfThread, exception));
            }
            HandledException(null, null);
        }

        /// <summary>
        /// Словарь ошибок, используемый для их подсчёта
        /// </summary>
        Dictionary<string, int> errors = new Dictionary<string, int>();

        /// <summary>
        /// Обработчик события поимки исключения.
        /// </summary>
        private void OnExceptionCatched(object sender = null, ExceptionCatchedEvent ef = null)
        {
            ExceptionCatchedEvent exception;
            lock (_locker)
            {
                exception = eventsQueue.Dequeue();
            }
            richTextBoxConsole.InvokeIfRequired(o =>
            {
                appendTextToRichTextBox(string.Format("{0} {1} {2}: {3}{4}", DateTime.Now.ToString("HH:mm:ss"), exception.ThreadName, exception.CurrentException.GetType(), exception.CurrentException.Message, Environment.NewLine), System.Drawing.Color.Red);
            }
            );

            try
            {
                errors[exception.CurrentException.GetType().ToString()] += 1;

            }
            catch
            {
                errors.Add(exception.CurrentException.GetType().ToString(), 0);
            }
            listViewErrors.InvokeIfRequired(o =>
            {
                o.Items.Clear();
                foreach (var pair in errors)
                {
                    o.Items.Add(pair.ToString());
                }
            }
            );
        }

        #endregion
    }
}
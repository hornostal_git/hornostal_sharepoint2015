﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System.ComponentModel;

namespace Task07_final_UI
{
    // Класс, содержащий методы расширения для синхронизации потоков.
    static class ExtensionMethods
    {
        // Делегат для действия, которое необходимо выполнить относительно UI из других потоков
        public delegate void InvokeIfRequiredDelegate<T>(T obj)
        where T : ISynchronizeInvoke;

        // Метод расширения, используемый для вывода обращения к элементам UI из других потоков
        public static void InvokeIfRequired<T>(this T obj, InvokeIfRequiredDelegate<T> action)
            where T : ISynchronizeInvoke
        {
            if (obj.InvokeRequired)
            {
                try
                {
                    obj.Invoke(action, new object[] { obj });
                }
                catch { }
            }
            else
            {
                action(obj);
            }
        }
    }
}

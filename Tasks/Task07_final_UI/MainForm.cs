﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Task07_final_UI
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            Iter(new DirectoryInfo(@"D:\Nix\SharePoint2015\Tasks\Task06_Core"));
            show();
        }

        Queue<string> queue = new Queue<string>();
        TreeNodeCollection collec;

        void Iter(DirectoryInfo dirs)
        {
            if (dirs.GetDirectories().Count() == 0)
                return;
            foreach (var dir in dirs.EnumerateDirectories())
            {
                queue.Enqueue(dir.FullName);
                Iter(dir);
            }
        }

        void show()
        {
            while (queue.Count != 0)
            {
                string currentDirName = queue.Dequeue();
                int index = currentDirName.IndexOf("Task06_Core");
                currentDirName = currentDirName.Substring(index, currentDirName.Length - index);
                string[] dirs = currentDirName.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                collec = treeViewMain.Nodes;
            }
        }

        void findrec(string[] arr)
        {
            TreeNodeCollection tempCollec = collec;
            int index = 0;
            bool end = false;
            bool sholContinue = false;

            while (tempCollec.Count != 0 && !end)
            {
                sholContinue = false;
                foreach (TreeNode n in tempCollec)
                {
                    if (n.Text == arr[index])
                    {
                        tempCollec = n.Nodes;
                        index++;
                        sholContinue = true;
                    }
                }
                if (sholContinue)
                    continue;
                else end = true;
            }


            for (int i = index; i < arr.Length; i++)
            {
                TreeNode tempNode = tempCollec.Add(arr[i]);
                tempCollec = tempNode.Nodes;
            }
        }
    }
}

﻿/**
 * @author Горносталь Алексей Андреевич
 */

using System;

/// <summary>
/// Содержит решение задания по теме "Циклы, коллекции, Generic типы, Generic коллекции" #1 (циклы).
/// </summary>
namespace Task02_a_Cycles
{
    class Program
    {
        /// <summary>
        /// Сторона квадрата таблицы умножения (количество элементов в строке и столбце). 
        /// </summary>
        const int N = 10;

        static void Main(string[] args)
        {
            ShowMultiplicationTableFor();
            ShowMultiplicationTableWhile();
            ShowMultiplicationTableDoWhile();
            ShowMultiplicationTableForeach();

            Console.ReadKey();
        }

        /// <summary>
        /// Выводит на экран таблицу умнжения с помощью цикла for.
        /// </summary>
        private static void ShowMultiplicationTableFor()
        {
            OutputTitle("for");
            for (int i = 1; i <= N; i++)
            {
                for (int n = 1; n <= N; n++)
                {
                    OutputNumber(i, n);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Выводит на экран таблицу умнжения с помощью цикла while.
        /// </summary>
        private static void ShowMultiplicationTableWhile()
        {
            OutputTitle("while");
            int n;
            int i = 1;
            while (i <= N)
            {
                n = 1;
                while (n <= N)
                {
                    OutputNumber(i, n);
                    n++;
                }
                Console.WriteLine();
                i++;
            }
        }

        /// <summary>
        /// Выводит на экран таблицу умнжения с помощью цикла do-while.
        /// </summary>
        private static void ShowMultiplicationTableDoWhile()
        {
            OutputTitle("do-while");
            int n;
            int i = 1;
            do
            {
                n = 1;
                do
                {
                    OutputNumber(i, n);
                    n++;
                }
                while (n <= N);
                Console.WriteLine();
                i++;
            }
            while (i <= N);
        }

        /// <summary>
        /// Выводит на экран таблицу умнжения с помощью цикла foreach.
        /// </summary>
        private static void ShowMultiplicationTableForeach()
        {
            int[] numbers = new int[N];

            for (int i = 0; i < N; i++)
            {
                numbers[i] = i;
            }

            OutputTitle("foreach");
            foreach (var i in numbers)
            {
                foreach (var n in numbers)
                {
                    OutputNumber(i, n);
                }
                Console.WriteLine();
            }
        }

        /// <summary>
        /// Выводит на экран заголовок с для описания проверяемого цикла.
        /// </summary>
        /// <param name="cycleName"> Имя цикла </param>
        private static void OutputTitle(string cycleName)
        {
            Console.WriteLine("\n This is an output for the '{0}' cycle:\n", cycleName);
        }

        /// <summary>
        /// Выводит число на экран.
        /// Если по диагонали, то выводится зелёным цветом.
        /// </summary>
        /// <param name="i"> Первый множитель </param>
        /// <param name="n"> Второй множитель </param>
        private static void OutputNumber(int i, int n)
        {
            bool isDiagonal = i == n;
            if (isDiagonal) Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("{0,-4}", i * n);
            if (isDiagonal) Console.ResetColor();
        }
    }
}
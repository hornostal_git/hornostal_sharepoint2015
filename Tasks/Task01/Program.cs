﻿/**
 * @author Горносталь Алексей Андреевич
 */

/// <summary>
/// Содержит тестовый набор классов для анализа кода с помощью ildasm.exe и .NetReflector.
/// </summary>
namespace Task01
{
    class Program
    {
        static void Main(string[] args)
        {
            MyStudentsCollection students = new MyStudentsCollection();
            students.Push(new Student { Name = "Alex", Surname = "Gornostal", Raiting = 97, Mark = Marks.A });
            students.Push(new Student { Name = "Anton", Surname = "Ivanov", Raiting = 87, Mark = Marks.B });
            students.Push(new Student { Name = "Maxim", Surname = "Petrenko", Raiting = 69, Mark = Marks.E });
            System.Console.WriteLine("Students` amount: " + students.Count);
            int amount = students.Count;
            for (int i = 0; i < amount; i++)
                System.Console.WriteLine("{0}: {1}",i+1,students.Pop());
            System.Console.ReadKey();
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;

namespace Task01
{
    class MyStudentsCollection
    {
        private List<Student> students;

        public MyStudentsCollection()
        {
            students = new List<Student>();
        }

        public int Count {
            get { return students.Count(); }
        }

        public void Push(Student student)
        {
            students.Add(student);
        }

        public Student Pop()
        {
            Student student = students.Last();
            students.RemoveAt(students.Count-1);
            return student;
        }
    }
}

﻿using System;

namespace Task01
{
    /// <summary>
    /// Оценки в международном буквенном формате.
    /// </summary>
    enum Marks
    {
        A,
        B,
        C,
        D,
        E,
        F,
        FX
    }

    /// <summary>
    /// Студент ВУЗа.
    /// </summary>
    class Student
    {
        /// <summary>
        /// Рейтинг по 100-бальной шкале
        /// </summary>
        private int _raiting;

        /// <summary>
        /// Имя студента.
        /// </summary>
        public string Name;
        /// <summary>
        /// Фамилия студента.
        /// </summary>
        public string Surname;
        /// <summary>
        /// Оценка студента в бувенном формате.
        /// </summary>
        public Marks Mark;

        /// <summary>
        /// Обеспечивает доступ к рейтингу студента.
        /// </summary>
        public int Raiting
        {
            get { return _raiting; }
            set
            {
                if (value >= 0 && value <= 100)
                    _raiting = value;
                else
                    throw new Exception("Raiting should be from 0 to 100!");
            }
        }
                
        /// <summary>
        /// Представляет информацию о студенте в виде текстовой строки.
        /// </summary>
        /// <returns> Тектсовое представление информации о студенте. </returns>
        public override string ToString()
        {
            return string.Format("{0,-10}{1,-12}{2,-5}{3}",Name,Surname,Raiting,Mark);
        }
    }
}

﻿/**
 * @author Горносталь Алексей Андреевич
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Windows.Forms;

/// <summary>
/// Содержит решение задания по теме "Делегаты, события, многопоточность".
/// </summary>
namespace Task05_Delegates_Events_Multithreading
{
    public partial class MainForm : Form
    {
        #region Внутренние поля

        /// <summary>
        /// Объект для блокировки участки кода.
        /// </summary>
        private readonly object _locker = new object();

        /// <summary>
        /// Первый поток.
        /// </summary>
        Thread thread1;

        /// <summary>
        /// Второй поток.
        /// </summary>
        Thread thread2;

        /// <summary>
        /// Потоки должны работать?
        /// </summary>
        bool _shouldWork = true;

        /// <summary>
        /// Период вызова исключений.
        /// </summary>
        private int _period = 200;

        /// <summary>
        /// Контекст для обновления состояния элементов пользовательского интерфейса из другого потока.
        /// </summary>
        private SynchronizationContext syncContext;

        /// <summary>
        /// Должен ли 1-й поток выбрасывать исключения?
        /// </summary>
        private static bool Thread1Throws = false;

        /// <summary>
        /// Должен ли 2-й поток выбрасывать исключения?
        /// </summary>
        private static bool Thread2Throws = false;

        /// <summary>
        /// Очередь исключений.
        /// </summary>
        private static Queue<ExceptionCatchedEvent> eventsQueue = new Queue<ExceptionCatchedEvent>();

        /// <summary>
        /// Событие для обработки исключения и последующего вывода сообщения.
        /// </summary>
        public event EventHandler<ExceptionCatchedEvent> HandledException;

        #endregion

        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Обрабатывает событие "загрузка окна" и обеспечивает запуск вторичных потоков.
        /// </summary>
        private void MainForm_Load(object sender, EventArgs e)
        {
            HandledException += OnExceptionCatched;
            syncContext = SynchronizationContext.Current;
            thread1 = new Thread(Thread1ExceptionCreator);
            thread2 = new Thread(Thread2ExceptionCreator);
            thread1.IsBackground = true;
            thread2.IsBackground = true;
            thread1.Start();
            thread2.Start();
        }

        #region Методы

        /// <summary>
        /// Метод для выброса изключения из первого потока.
        /// </summary>
        void Thread1ExceptionCreator()
        {
            while (_shouldWork)
            {
                try
                {
                    if (Thread1Throws)
                        throw new FileNotFoundException();
                }
                catch (Exception e)
                {
                    InvokeExceptionHandler(e, Thread.CurrentThread.Name);
                }
                Thread.Sleep(_period);
            }
        }

        /// <summary>
        /// Метод для выброса изключения из второго потока.
        /// </summary>
        void Thread2ExceptionCreator()
        {
            while (_shouldWork)
            {
                try
                {
                    if (Thread2Throws)
                        throw new OutOfMemoryException();
                }
                catch (Exception e)
                {
                    InvokeExceptionHandler(e, Thread.CurrentThread.Name);
                }
                Thread.Sleep(_period);
            }
        }

        /// <summary>
        /// Вызывается при поимке исключения.
        /// </summary>
        /// <param name="exception"> Выброшенное исключение. </param>
        /// <param name="nameOfThread"> Имя потока с исключением. </param>
        private void InvokeExceptionHandler(Exception exception, string nameOfThread)
        {
            lock (_locker)
            {
                eventsQueue.Enqueue(new ExceptionCatchedEvent(nameOfThread, exception));
            }
            HandledException(null, null);
        }

        /// <summary>
        /// Содержит информацию о пойманном исключении.
        /// </summary>
        public class ExceptionCatchedEvent : EventArgs
        {
            public ExceptionCatchedEvent(string threadName, Exception exception)
            {
                ThreadName = threadName;
                CurrentException = exception;
            }

            /// <summary>
            /// Имя потока, в котором поймано исключение.
            /// </summary>
            public string ThreadName { get; set; }

            /// <summary>
            /// Текущее исключение в потоке.
            /// </summary>
            public Exception CurrentException { get; set; }
        }

        /// <summary>
        /// Обработчик события поимки исключения.
        /// </summary>
        private void OnExceptionCatched(object sender = null, ExceptionCatchedEvent ef = null)
        {
            syncContext.Post(Update, null);
        }

        /// <summary>
        /// Обеспечивает вывод сообщения о последнем исключении.
        /// </summary>
        private void Update(object state)
        {
            lock (_locker)
            {
                ExceptionCatchedEvent e = eventsQueue.Dequeue();
                textBox1.AppendText(string.Format("{0} {1}: {2}{3}", DateTime.Now.ToString("HH:mm:ss"), e.ThreadName, e.CurrentException.GetType(), Environment.NewLine));
            }
        }

        #endregion

        #region Обработчики событий окна

        /// <summary>
        /// Обеспечивает включение и выключение выбрасывания исключений из потока №1.
        /// </summary>
        private void button1_Click(object sender, EventArgs e)
        {
            Thread1Throws =
                Thread1Throws == true
                    ? false
                    : true;
        }

        /// <summary>
        /// Обеспечивает включение и выключение выбрасывания исключений из потока №2.
        /// </summary>
        private void button2_Click(object sender, EventArgs e)
        {
            Thread2Throws =
                Thread2Throws == true
                    ? false
                    : true;
        }

        /// <summary>
        /// Обеспечивает обработку изменения периода выбрасывания исключений.
        /// </summary>
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            _period = int.Parse(comboBox1.SelectedItem.ToString());
        }

        /// <summary>
        /// Обеспечивает корректное завершение потоков после нажатия кнопки Х.
        /// </summary>
        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _shouldWork = false;
            Thread1Throws = false;
            Thread2Throws = false;
            thread1.Join();
            thread2.Join();
        }

        #endregion
    }
}

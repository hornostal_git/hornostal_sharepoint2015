﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CartSummary.ascx.cs" Inherits="SportsStore.Pages.CartSummary" %>
<div id="cartSummary">
    <span class="caption">
        <b>Your cart:</b>
        <span id="csQuantity" runat="server"></span> item(s),
        <span id="csTotal" runat="server"></span>
    </span>
    <a id="csLink" runat="server" href="checkout">Checkout</a>
</div>
﻿using System;
using System.Web.Security;

namespace SportsStore.Pages
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack)
            {
                string name = Request.Form["name"];
                string pass = Request.Form["password"];

                if (name != null && pass != null && FormsAuthentication.Authenticate(name, pass))
                {
                    FormsAuthentication.SetAuthCookie(name, false);
                    Response.Redirect(Request["ReturnUrl"] ?? "/");
                }
                else
                    ModelState.AddModelError("fail","Login faild. Please try again!");
            }
        }
    }
}
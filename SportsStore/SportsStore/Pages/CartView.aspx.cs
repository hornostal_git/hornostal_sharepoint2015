﻿using SportsStore.Models;
using SportsStore.Models.Repository;
using SportsStore.Pages.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SportsStore.Pages
{
    public partial class CartView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(IsPostBack)
            {
                Repository repo = new Repository();
                int selectedProductID;
                if(int.TryParse(Request.Form["remove"],out selectedProductID))
                {
                    Product selectedProduct = repo.Products.Where(p => p.ProductID == selectedProductID).FirstOrDefault();
                    if(selectedProduct!=null)
                    {
                        SessionHelper.GetCart(Session).RemoveLine(selectedProduct);
                    }
                }
            }
        }

        public IEnumerable<CartLine> GetCartLines()
        {
            return SessionHelper.GetCart(Session).Lines;
        }

        public decimal CartTotal
        {
            get
            {
                return SessionHelper.GetCart(Session).ComputeTotalValue();
            }
        }

        public string ReturnUrl()
        {
            return SessionHelper.Get<string>(Session, SessionKey.RETURN_URL);
        }

        public string CheckoutUrl()
        {
            return RouteTable.Routes.GetVirtualPath(null, "checkout", null).VirtualPath;
        }
    }
}